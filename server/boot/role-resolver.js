var debug = require('debug')('loopback:security:role-resolver');

module.exports = function(app) {
  var Role = app.models.Role;

  Role.registerResolver('authorisedEditor', function(role, context, cb){
    debug('in role authorisedEditor')
    function reject() {
      // console.log('rejected inside')
      process.nextTick(function() {
        cb(null, false);
      });
    }
    function accept() {
      // console.log('rejected inside')
      process.nextTick(function() {
        cb(null, true);
      });
    }



    var userId = context.accessToken.userId;
    if (!userId) {
      debug('rejected anonymous user!');
      return reject();
    }
    else if(!(context.modelId)){
      debug('trying to create new protocol')

      var data = context.remotingContext.args.data;

      if(String(data.ownerType) == "user"){
        if(String(data.ownerId) == String(userId)){
          debug('user is allowed to create protocol for himself')
          accept();
        }
        else{
          debug('user cannot create protocol for someone else')
          reject();
        }
      }
      else if(String(data.ownerType) == "team"){

        debug('will check if it is user\'s team')

        context.model.app.models.user.findById(userId,
        {include: ['joinedTeam', 'ownedTeam']},
        function(error, userResponse){
          if(error){
            debug('problem with getting userdata');
            reject();
          }
          else{

            var userData = userResponse.toJSON();
            var teamIds = userData.joinedTeam.map(function(obj){
              return String(obj.id);});
            teamIds = teamIds.concat(userData.ownedTeam.map(function(obj){
              return String(obj.id);}));

            if(teamIds.indexOf(String(data.ownerId)) > -1){
              debug('user is creating protocol for his team')
              accept();
            }
            else{
              debug('not your team')
              reject();
            }

          }
        })


      }
      else{
        debug('invalid ownerType');
        reject();
      }

    }
    else{
      debug('user is updating a protocol, check if OK');

      context.model.app.models.Protocol.findById(context.modelId,
        function(error,protocol){
          if(error){
            debug('error with protocol retrieval/doesn\'t exist')
            reject();
          }
          else{
            debug('protocol retrieved')

            if(String(protocol.ownerType) == 'team' ){
              debug('it is a team protocol')

              context.model.app.models.user.findById(userId,
              {include: ['joinedTeam', 'ownedTeam']},
              function(error, userResponse){
                if(error){
                  debug('problem with getting userdata');
                  reject();
                }
                else{

                  var userData = userResponse.toJSON();
                  var teamIds = userData.joinedTeam.map(function(obj){
                    return String(obj.id);});
                  teamIds = teamIds.concat(userData.ownedTeam.map(function(obj){
                    return String(obj.id);}));

                  debug(teamIds)
                  debug(protocol.ownerId)

                  if(teamIds.indexOf(String(protocol.ownerId)) > -1){
                    accept();
                  }
                  else{
                    debug('not your team')
                    reject();
                  }

                }
              })

            }
            else{
              debug('it is a user protocol')
              if(String(protocol.ownerId) == String(userId)){
                debug('it is user\'s protocol')
                accept();
              }
              else{
                debug('user not allowed to edit other user\'s protocol')
                reject();
              }
            }


          }


        })

    }

  });

  Role.registerResolver('teamMember', function(role, context, cb) {
    function reject() {
      // console.log('rejected inside')
      process.nextTick(function() {
        cb(null, false);
      });
    }

    debug('here we call the role!')
    //console.log(context)

    // if the target model is not team
    if (context.modelName !== 'team') {
       debug('rejected model!')
      return reject();
    }

    // do not allow anonymous users
    var userId = context.accessToken.userId;
    if (!userId) {
      debug('rejected anonymous user!')
      return reject();
    }

    // check if userId is in team table for the given team id
    context.model.findById(context.modelId, function(err, team) {
      
      if (err || !team){
        debug('rejected down!')
        return reject();
      }

      team.members.exists(userId, function(err, count) {
        console.log(count > 0);
        if (err) {
          console.log('err');
          console.log(err);
          return cb(null, false);
        }

        cb(null, count > 0); // true = is a team member
      });
    });
  });

  Role.registerResolver('pictureOwner', function(role, context, cb) {

    var User = app.models.User;

    function reject() {
      // console.log('rejected inside')
      process.nextTick(function() {
        cb(null, false);
      });
    }

    debug('here we call the role!')
    //console.log(context)

    // if the target model is not team
    if (context.modelName !== 'Container') {
       debug('rejected model!')
      return reject();
    }

    // do not allow anonymous users
    var userId = context.accessToken.userId;
    if (!userId) {
      debug('rejected anonymous user!')
      return reject();
    }

    // check if the user owns the profile picture container
    User.findOne({where: {pictureContainer: context.modelId}}, function(err, count) {
      
      if (err || !count){
        debug('rejected down!')
        return reject();
      }

      cb(null, count > 0); // true = is an owner
      
    });
  });

}