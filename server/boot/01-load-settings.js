'use strict';

// to enable these logs set `DEBUG=boot:01-load-settings` or `DEBUG=boot:*`
var log = require('debug')('boot:01-load-settings');

module.exports = function(app) {

  var Setting = app.models.Setting;

  function loadDefaultSettings() {
    console.error('Creating default settings');

    var settings = [{
      type: 'string',
      key: 'appName',
      value: 'scitocols'
    }, {
      type: 'select',
      key: 'appTheme',
      value: 'skin-blue'
    }, {
      type: 'select',
      key: 'appLayout',
      value: 'layout-top-nav'
    }, {
      type: 'string',
      key: 'formLayout',
      value: 'horizontal'
    }, {
      type: 'int',
      key: 'formLabelSize',
      value: 3
    }, {
      type: 'int',
      key: 'formInputSize',
      value: 9
    }, {
      type: 'boolean',
      key: 'com.module.users.enable_registration',
      value: true
    }];

    settings.forEach(function(setting) {
      Setting.create(setting, function(err) {
        if (err) {
          console.error(err);
        }
      });
    });
  }

  function loadExistingSettings() {
    console.error('Loading existing settings');

    Setting.find(function(data) {
      log(data);
    });
  }


  Setting.count(function(err, result) {
    if (err) {
      console.error(err);
    }
    if (result < 1) {
      loadDefaultSettings();
    } else {
      loadExistingSettings();
    }
  });


  //add photos for all users and protocols to have them all
  function gerneratePhotos(){
    var Trianglify = require('trianglify');
    var AWS = require('aws-sdk');
    var AWSconfig = require('../../server/datasources.json');
    AWS.config.update({accessKeyId: AWSconfig.amazonS3.keyId, secretAccessKey: AWSconfig.amazonS3.key});
  
    //do it once for users
    var user = app.models.user;
    var s3Bucket1 = new AWS.S3( { params: {Bucket: 'scitocols-profile-photos'} } );
  
    user.find({}, function(err, accounts) {
      if(err){
        console.log(err);
      }else{
        var acc;
        var i=0;
        accounts.forEach(function(acc) {
          i=i+1;
          console.log("user number",i);
          console.log("user id",acc);
          var pattern = Trianglify({width: 150, height: 150, seed: acc.id}).png();
          var pic = pattern.substr(pattern.indexOf('base64') + 7);
          var buffer = new Buffer(pic, 'base64');
  
          //unfortunately we have to go with direct S3 access
          
          
  
          var data = {
              Key: acc.id+".jpg", 
              Body: buffer,
              ContentEncoding: 'base64',
              ContentType: 'image/jpeg'
            };
            s3Bucket1.putObject(data, function(err, data){
                if (err) { 
                  console.log(err);
                  console.log('Error uploading data: ', data); 
                } else {
                  console.log('succesfully uploaded the image!');
                }
            });
  
        });
  
  
      }
  
  
    });
  
    //and now the same for protocols:
    var Protocol = app.models.Protocol;
    var s3Bucket2 = new AWS.S3( { params: {Bucket: 'scitocols-protocol-coverphotos'} } );
  
    Protocol.find({}, function(err, accounts2) {
      if(err){
        console.log(err);
      }else{
        var acc2;
        var j=0;
        accounts2.forEach(function(acc2) {
          j=j+1;
          console.log("protocol number",j);
          console.log("protocol id",acc2.id);
          var pattern = Trianglify({width: 600, height: 300, seed: acc2.id}).png();
          var pic = pattern.substr(pattern.indexOf('base64') + 7);
          var buffer = new Buffer(pic, 'base64');
  
          //unfortunately we have to go with direct S3 access
          
          
  
          var data = {
              Key: acc2.id+".jpg", 
              Body: buffer,
              ContentEncoding: 'base64',
              ContentType: 'image/jpeg'
            };
            s3Bucket2.putObject(data, function(err, data){
                if (err) { 
                  console.log(err);
                  console.log('Error uploading data: ', data); 
                } else {
                  console.log('succesfully uploaded the image!');
                }
            });
  
        });
  
  
      }
  
  
    });
  
    console.log("done gernating all the images!");
  }

  //only run when needed again..
  //gerneratePhotos();



};
