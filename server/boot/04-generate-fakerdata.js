'use strict';

// to enable these logs set `DEBUG=boot:02-load-users` or `DEBUG=boot:*`
var log = require('debug')('boot:04-generate-fakerdata');
var faker = require('faker');
var Q = require('q');
var _ = require('underscore');

module.exports = function(app) {

  // if (app.dataSources.db.name !== 'Memory' && !process.env.INITDB) {
  if (!process.env.INITDB) {
    console.log('Not generating data')
   return;
  }

  console.log('Generating data')

  var Team = app.models.Team;
  var User = app.models.User
  var Token = app.models.accessToken;
  var Protocol = app.models.Protocol;
  var Step = app.models.Step;
  var Comment = app.models.Comment;
  var Commit = app.models.Commit;
  var Tag = app.models.Tag;

  var Nusers = 100;
  var Nteams = 10;
//  var users = [];



  var deletePromise = deleteAllData();
  var tagsPromise = deletePromise.then(function(delResponse){
    return createTags();
  });
  var usersPromise = deletePromise.then(function(delResponse){
    return generateUsers();
  });
  var teamsPromise = usersPromise.then(function(users){
    return generateTeams(users);
  });

  var teamMembersPromise = Q.all([usersPromise, teamsPromise]).then(function(response){
    return addTeamMembers(response);
  })

  var protocolsPromise = Q.all([teamsPromise, tagsPromise]).then(function(response){
    return generateTeamProtocols(response);
  })

  var createdProtocolsPromise = protocolsPromise.then(function(val) {return getAllProtocols();});
  var commentsPromise = Q.all([usersPromise, createdProtocolsPromise]).then(function(response){
    return generateComments(response);
  })
 
  Q.all([teamMembersPromise, protocolsPromise, commentsPromise]).done(function(response){console.log('Done generating data')});


  function deleteAllData() {

    console.log('in delete')

    return Q.all([
    Token.destroyAll(),
    Team.destroyAll(),
    Protocol.destroyAll(),
    Comment.destroyAll(),
    Commit.destroyAll(),
    Tag.destroyAll()
    ]);

  };

  function createTags() {

    var tagtext = ["physics", "photonics", "fabrication", "cleaning", "perovskite", "EBL", "ZnO"];
    var tagSubmit = tagtext.map(function(obj) {return {text: obj};});

    return Tag.create(tagSubmit);
  }

  function generateUsers() {

    var newUserPromises = [];

    for(var i = 0; i < Nusers; i++){

      var firstName = faker.name.firstName();
      var lastName = faker.name.lastName();
      var email = firstName+"."+lastName+"@email.com";
      var username = firstName +'.'+lastName;
      var password = faker.internet.password();

      var submit = {'email': email, 'username': username, 'firstName': firstName, 'lastName': lastName, 'password': password};

      newUserPromises.push(User.create(submit));

    }

    return Q.all(newUserPromises);

  }

  function generateTeams(users){

      var teamsPromises = [];

      for(var i = 0; i < Nteams; i++){

        var randomOwner = Math.round(Math.random()*(Nusers-1));
        var teamName = users[randomOwner].firstName+"'s "+faker.hacker.adjective()+" team";
        var submit = {'name': teamName, 'ownerId': users[randomOwner].id};

        teamsPromises.push(Team.create(submit));

      }

      return Q.all(teamsPromises);

  }

  function addTeamMembers(data){

    var users = data[0];
    var teams = data[1];

    var memberPromises = [];
    for(var i in teams){
      for (var j = 0; j < 5; j++){
        var randomMember = Math.round(Math.random()*(Nusers-1));
        memberPromises.push(teams[i].members.add(users[randomMember].id));
      }
    }
    return Q.all(memberPromises);
  }

  function generateTeamProtocols(data){

    var teams = data[0];
    var tags = data[1];

    var protocolPromises = [];

    for(var i in teams){
      for(var j = 0; j < 5; j++){

        var protocol = {'title': faker.lorem.sentence(), 'description': faker.lorem.paragraph(), 
                        'ownerType': 'team', 'ownerId': teams[i].id, 'protocolTags':[], 'protocolSteps':[]};

        //Generate steps
        for (var k = 0; k < 3; k++){
          protocol.protocolSteps.push({'text': faker.lorem.paragraph(), 'order': k});
        };

        //Select tags
        protocol.protocolTags = _.sample(tags,3);

        // protocolPromises.push(Protocol.upsertProtocolWithSteps(protocol, protocolTags, protocolSteps, 'First commit'));
        protocolPromises.push(Protocol.upsert(protocol));
      }
    }

    return Q.all(protocolPromises);

  }

  function getAllProtocols(){
    return Protocol.find({include:'steps'});
  }

  function generateComments(data){
    var users = data[0];
    var protocols = data[1];

    var commentsPromises = [];

    for(var i in protocols){

      var protocol = JSON.parse(JSON.stringify(protocols[i]));
      
      var Ncomments = Math.round(Math.random()*3)+1;


      for(var j=0; j<Ncomments; j++){
        var user = _.sample(users)
        var protocolComment = {'text': faker.lorem.sentence(), 'commentableType':'protocol', 'commentableId': protocol.id, 'userId': user.id};
        commentsPromises.push(Comment.create(protocolComment));
      }
      var step = _.sample(protocol.steps);
      var Ncomments2 = Math.round(Math.random()*2)+1;
      for(var j=0; j<Ncomments2; j++){
        var user = _.sample(users)
        var stepComment = {'text': faker.lorem.sentence(), 'commentableType':'step', 'commentableId': step.id, 'userId': user.id};
        commentsPromises.push(Comment.create(stepComment));
      }



    }

    return Q.all(commentsPromises);
  }

};