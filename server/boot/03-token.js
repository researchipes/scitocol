'use strict';

// to enable these logs set `DEBUG=boot:01-load-settings` or `DEBUG=boot:*`
var log = require('debug')('boot:03-token');
var loopback = require('loopback');

module.exports = function(app) {
	// looback.context() allows us to access user info on the server side
	app.use(loopback.context());
	app.use(loopback.token({
	  model: app.models.accessToken
	}))

	app.use(function setCurrentUser(req, res, next) {
	  if (!req.accessToken) {
	    return next();
	  }
	  app.models.user.findById(req.accessToken.userId, function(err, user) {
	    if (err) {
	      return next(err);
	    }
	    if (!user) {
	      return next(new Error('No user with this access token was found.'));
	    }
	    var loopbackContext = loopback.getCurrentContext();
	    if (loopbackContext) {
	      loopbackContext.set('currentUser', user);
	    }
	    next();
	  });
	});


};