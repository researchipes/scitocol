var _ = require('underscore');

//Checks the intersection of two arrays of objects
function intersectionObjects2(a, b, areEqualFunction){
  var results = [];

  for (var i = 0; i < a.length; i++) {
    var aElement = a[i];
    var existsInB = _.any(b, function(bElement){
      return areEqualFunction(bElement, aElement);
    });

    if (existsInB) {
      results.push(aElement);
    }
  }

  return results;
}

// Checks the intersection of arrays of objects
// (modelArrays is an array of arrays)
exports.intersectionObjects = function(modelArrays){

  var results = modelArrays[0];
  var arrayCount = modelArrays.length;

  //This will work for most of our models, just check equality by id
  var areEqualFunction = function(elA, elB){
    return String(elA.id) == String(elB.id);
  };

  for (var i = 1; i < arrayCount ; i++) {
    var array = modelArrays[i];
    results = intersectionObjects2(results, array, areEqualFunction);
    if (results.length === 0) {break;}
  }

  return results;
};
