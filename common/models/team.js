module.exports = function(Team){

  Team.observe('before save', function(ctx, next){

    if (ctx.instance) {
      ctx.instance.created = Date.now();

      //If there are memberIds in the instance object, move it into the ctx for later
      if (ctx.instance.memberIds) {

        ctx.hookState.memberIds = ctx.instance.memberIds;
        // delete ctx.instance.memberIds;
        ctx.instance.unsetAttribute('memberIds');
      }
    }
    else if (ctx.data) {

      if (ctx.data.memberIds) {

        ctx.hookState.memberIds = ctx.data.memberIds;
        // delete ctx.instance.memberIds;
        delete ctx.data.memberIds;
      }
    }

    next();
  });

  Team.observe('after save', function(ctx, next){

    if (ctx.hookState.memberIds) {
      //For some reason team.members.add() does not take an array of new members

      ctx.instance.members(function(err, data){

        var oldMembers = data;
        var oldMemberIds = oldMembers.map(function(obj){return obj.id;});
        var newMemberIds = ctx.hookState.memberIds;

        //Remove old members that are not in the new member list
        for (var idx in oldMemberIds) {

          if (newMemberIds.indexOf(oldMemberIds[idx]) < 0) {
            ctx.instance.members.remove(oldMemberIds[idx],
              function(err, data){});
          }

        }

        //Add new members that are not in the old member list
        for (var idx in newMemberIds) {

          if (oldMemberIds.indexOf(newMemberIds[idx]) < 0) {
            ctx.instance.members.add(newMemberIds[idx],
              function(err, data){});
          }
        }
      });
    }

    next();
  });
};
