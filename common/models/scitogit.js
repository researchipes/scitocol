var nodegit = require('nodegit');
var path = require('path');

// This code examines the diffs between a particular commit and all of its
// parents. Since this commit is not a merge, it only has one parent. This is
// similar to doing `git show`.

exports.gitShow = function(data, callback){
  var repoDir = '../../storage/repos/' + data.protocolId + '/.git';
  var repoLocation = path.resolve(__dirname, repoDir);
  var oid = data.oid;
  var output = {};
  nodegit.Repository.open(path.resolve(repoLocation))
    .then(function(repo){
      return repo.getCommit(oid);
    })
    .then(function(commit){
      output.commitInfo = {sha: commit.sha(),
        author: commit.author().name(),
        email: commit.author().email(),
        date: commit.date(),
        message: commit.message()};

      return commit.getDiff();
    })
    .then(function(diffList){
      output.changes = [];
      return Promise.all(diffList.map(function(diff){
        return diff.patches().then(function(patches){
          return Promise.all(patches.map(function(patch){
            return patch.hunks().then(function(hunks){
              return Promise.all(hunks.map(function(hunk){
                return hunk.lines().then(function(lines){
                  var linesChanged = [];
                  lines.forEach(function(line){
                    // Loop through the lines, and push to the array returned.
                    // We only care about added (code 43) or removed lines (code
                    // 45), not about 'No newline at end of file' messages.
                    var origin = line.origin();
                    if (origin == 43) {
                      linesChanged.push({lineClass: 'git-line-added',
                        lineContent: line.raw.content().substring(0,
                          line.raw.contentLen())});
                    }
                    else if (origin == 45) {
                      linesChanged.push({lineClass: 'git-line-removed',
                        lineContent: line.raw.content().substring(0,
                          line.raw.contentLen())});
                    }
                  });
                  output.changes.push({
                    oldFile: patch.oldFile().path(),
                    newFile: patch.newFile().path(),
                    hunkHeader: hunk.header().trim(),
                    linesChanged: linesChanged
                  });
                });
              }));
            });
          }));
        });
      }));
    })
    .done(function(){
      callback(output);
    });
};

exports.gitGetVersion = function(data, callback){
  var repoLocation = data.repoLocation;
  var oid = data.oid;
  var protocol;

  nodegit.Repository.open(path.resolve(repoLocation))
    .then(function(repo){
      return repo.getCommit(oid);
    })
    .then(function(commit){
      return commit.getTree();
    })
    .then(function(tree){
      entries = tree.entries();

      function filterByExtension(entry){
        if (entry.filename().slice(-4) === 'info') {
          return true;
        }
        else {
          return false;
        }
      }

      entries = entries.filter(filterByExtension);

      var filesPromises = entries.map(function(entry){
        return entry.getBlob().then(function(blob){
          fileName = entry.filename();
          if (fileName === 'protocol.info') {
            // We don't wont to return the protocol in the array of steps
            // Instead, we assign it to the variable we'll eventually return
            protocol =  JSON.parse(blob.toString());
            return '';
          }
          else {
            return JSON.parse(blob.toString().split('\n'));
          }
        });
      });

      return Promise.all(filesPromises);
    })
    .then(function(steps){
      // Throw away the empty step
      steps = steps.filter(function(step){
        // Since the protocol member of the steps array is empty, the step evaluates
        // to false, so won't be returned
        return step;
      });
      // Assign the steps if we have any left
      if (steps) {
        protocol.steps = steps;
      }
    })
    .done(function(){
      callback(protocol);
    });
};
