var loopback = require('loopback');

module.exports = function(Watcher){

  function getCurrentUserId(){
    var currCtx = loopback.getCurrentContext();
    var currentUser = currCtx && currCtx.get('currentUser');
    var userId = currentUser.id;
    return userId;
  }

  Watcher.observe('before save', function setCreatedDate(ctx, next){
    if (ctx.instance) {
      ctx.instance.created = Date.now();
    }
    next();
  });

  Watcher.toggleWatcher = function(protoId, cb){

    var userId = getCurrentUserId();
    var returnMsg = '';

    Watcher.count({'protocolId': protoId,'userId': userId})
    .then(function(response){
      return response > 0;
    })
    .then(function(watchedbyme){

      if (!watchedbyme) {

        var newWatcher = {
          protocolId: protoId,
          userId: userId
        };

        return Watcher.create(newWatcher)
          .then(function(result){
            returnMsg = 'Now watching!';
            cb(null, returnMsg);
          })
          .catch(function(error){
            console.log(error);
          });

      }
      else {
        return Watcher.destroyAll({'protocolId': protoId,'userId': userId})
        .then(function(result){
          returnMsg = 'No longer watching!';
          cb(null, returnMsg);
        })
        .catch(function(error){
          console.log(error);
        });
      }

    })
    .catch(function(error){
      returnMsg = error;
      console.log(error);
    });

  };

  Watcher.remoteMethod(
    'toggleWatcher',
        {
          accepts: {arg: 'protoId', type: 'string'},
          returns: {arg: 'returnMsg', type: 'string'}
        }
    );

};
