var loopback = require('loopback');

module.exports = function(Star){

  function getCurrentUserId(){
    var currCtx = loopback.getCurrentContext();
    var currentUser = currCtx && currCtx.get('currentUser');
    var userId = currentUser.id;
    return userId;
  }

  Star.observe('before save', function setCreatedDate(ctx, next){
    if (ctx.instance) {
      ctx.instance.created = Date.now();
    }
    next();
  });

  Star.toggleStar = function(protoId, cb){

    var userId = getCurrentUserId();
    var returnMsg = '';

    Star.count({'protocolId': protoId,'userId': userId})
    .then(function(response){
      return response > 0;
    })
    .then(function(starredbyme){

      if (!starredbyme) {

        var newStar = {
        protocolId: protoId,
        userId: userId
      };

        return Star.create(newStar)
        .then(function(result){
          returnMsg = 'Added star!';
          cb(null, returnMsg);
        })
        .catch(function(error){
          console.log(error);
        });

      }
      else {
        return Star.destroyAll({'protocolId': protoId,'userId': userId})
        .then(function(result){
          returnMsg = 'Removed star!';
          cb(null, returnMsg);
        })
        .catch(function(error){
          console.log(error);
        });
      }

    })
    .catch(function(error){
      returnMsg = error;
      console.log(error);
    });
  };

  Star.remoteMethod(
    'toggleStar',
        {
          accepts: {arg: 'protoId', type: 'string'},
          returns: {arg: 'returnMsg', type: 'string'}
        }
    );

};
