var scitogit = require('./scitogit.js');

module.exports = function(Commit){

  Commit.observe('before save', function setCreatedDate(ctx, next){
    if (ctx.instance) {
      ctx.instance.created = Date.now();
    }
    next();
  });

  Commit.gitShow = function(commit, cb){
      data = {protocolId: commit.protocolId, oid: commit.oid};
      scitogit.gitShow(data, function(gitdiff){
        cb(null, gitdiff);
      });
    };

  Commit.remoteMethod(
    'gitShow',
        {
          accepts: {arg: 'commit', type: 'object'},
          returns: {arg: 'gitdiff', type: 'Object'}
        }
    );

};
