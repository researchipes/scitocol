module.exports = function(Tag){

  Tag.observe('before save', function setCreatedDate(ctx, next){
    if (ctx.instance) {
      ctx.instance.created = Date.now();
    }
    next();
  });

};
