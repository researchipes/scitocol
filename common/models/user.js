var loopback = require('loopback');

module.exports = function(user){

  //verify the user
  user.afterRemote('create', function(context, user, next){
    //send verification link
    var options = {
      type: 'email',
      to: user.email,
      from: 'postmaster@mailer.scitocols.com',
      subject: 'Thanks for registering.',
      redirect: encodeURIComponent('/'),
      user: user
    };

    console.log('user',user.verify);
    user.verify(options, function(err, response){
      if (err) {
        next(err);
        return;
      }
      console.log('> verification email sent:');
      next();
    });
  });

  // Set the username to the users email address by default.
  user.observe('before save', function setDefaultUsername(ctx, next){
    if (ctx.instance) {
      if (ctx.instance.username === undefined) {
        ctx.instance.username = ctx.instance.email;
      }

      ctx.instance.status = 'created';
      ctx.instance.created = Date.now();
    }
    next();
  });

  user.observe('after save', function generateProfilephoto(ctx, next){
    if (ctx.instance.id && ctx.isNewInstance) {
      // console.log(ctx.instance.id);

      //create a profile image
      var Trianglify = require('trianglify');
      var pattern = Trianglify({width: 150, height: 150,
        seed: ctx.instance.id}).png();
      var pic = pattern.substr(pattern.indexOf('base64') + 7);
      var buffer = new Buffer(pic, 'base64');

      //unfortunately we have to go with direct S3 access
      var AWS = require('aws-sdk');
      var AWSconfig = require('../../server/datasources.json');
      AWS.config.update({accessKeyId: AWSconfig.amazonS3.keyId,
        secretAccessKey: AWSconfig.amazonS3.key});
      var s3Bucket = new AWS.S3({params: {Bucket: 'scitocols-profile-photos'}});

      var data = {
          Key: ctx.instance.id + '.jpg',
          Body: buffer,
          ContentEncoding: 'base64',
          ContentType: 'image/jpeg'
        };
      s3Bucket.putObject(data, function(err, data){
        if (err) {
          console.log(err);
          console.log('Error uploading data: ', data);
        }
        else {
          // console.log('succesfully uploaded the image!');
        }
      });

      ctx.instance.status = 'created';
      ctx.instance.created = Date.now();
    }
    next();
  });

  //send password reset link when requested
  user.on('resetPasswordRequest', function(info){

    user.findOne({where: {email: info.email}},function(err, theuser){

      if (theuser){
        // TODO: This should link to scitocols.com
        var url = 'http://0.0.0.0:9000/#' + '/resetpass';
        var html = 'Click <a href="' + url + '?access_token=' +
            info.accessToken.id + '">here</a> to reset your password';

        user.app.models.Email.send({
          to: info.email,
          from: 'postmaster@mailer.scitocols.com',
          subject: 'Password reset',
          html: html
        }, function(err){
          if (err){
            return console.log('> error sending password reset email', err);
          }
          console.log('> sending password reset email to:', info.email);
        });
      }

    });

  });

  user.getTeams = function(cb){

    var currCtx = loopback.getCurrentContext();
    var currentUser = currCtx && currCtx.get('currentUser');
    var currentUserId = currentUser.id;

    user.findById(currentUserId, {include: ['joinedTeam', 'ownedTeam']})
    .then(function(response){
      response = response.toJSON();
      cb(null, {joined: response.joinedTeam, owned: response.ownedTeam});
    })
    .catch(function(error){
      cb(error, null);
    });
  };

  user.remoteMethod(
      'getTeams',
      {
        accepts: [],
        returns: {arg: 'teams', type: 'Object'}
      }
  );

  user.saveNewPassword = function(access_token, newpassword, fn){ // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers

    var currCtx = loopback.getCurrentContext();
    var currentUser = currCtx && currCtx.get('currentUser');
    var currentUserId = currentUser.id;

    this.findById(currentUserId, function(err, user){
      if (err) {
        fn(err);
      }
      else {
        if (user) {
          user.password = newpassword;
          user.save(function(err){
            if (err) {
              fn(err);
            }
            else {
              fn(null,user);
            }
          });
        }
        else {
          if (user) {
            err = new Error('Invalid token: ');
            err.statusCode = 400;
            err.code = 'INVALID_TOKEN';
          }
          else {
            err = new Error('User not found: ' + currentUserId);
            err.statusCode = 404;
            err.code = 'USER_NOT_FOUND';
          }
          fn(err);
        }
      }
    });
  };

  user.remoteMethod(
    'saveNewPassword',
    {
      description: 'Save a password',
      accepts: [
        {arg: 'access_token', type: 'string', required: true},
        {arg: 'newpassword', type: 'string', required: true}
      ],
      http: {verb: 'get', path: '/savenewpass'},
      returns: {arg: 'changeduser', type: 'string'}
    }
  );

};
