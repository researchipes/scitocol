// Showdown is used for Markdown parsing, will be useful later!
var Showdown = require('showdown');
var nodegit = require('nodegit');
var async = require('async');
var path = require('path');
var fse = require('fs-extra-promise');
var loopback = require('loopback');
var debug = require('debug')('loopback:models:protocol');
var scitogit = require('./scitogit.js');
var intersection = require('./intersection.js');
var _ = require('underscore');
var underscoreDeepExtend = require('underscore-deep-extend');
_.mixin({deepExtend: underscoreDeepExtend(_)});

module.exports = function(Protocol){

  Protocol.observe('before save', function moveStuff(ctx, next){

    // ctx.instance is only defined if it's a new instance
    if (ctx.instance) {
      ctx.instance.created = Date.now();
      ctx.instance.modified = Date.now();

      if (ctx.instance.protocolSteps) {
        ctx.hookState.steps = ctx.instance.protocolSteps;
        ctx.instance.unsetAttribute('protocolSteps');
      }
      if (ctx.instance.protocolTags) {
        ctx.hookState.tags = ctx.instance.protocolTags;
        ctx.instance.unsetAttribute('protocolTags');
      }
      if (ctx.instance.protocolMaterials) {
        ctx.hookState.materials = ctx.instance.protocolMaterials;
        ctx.instance.unsetAttribute('protocolMaterials');
      }
      if (ctx.instance.protocolEquipment) {
        ctx.hookState.equipment = ctx.instance.protocolEquipment;
        ctx.instance.unsetAttribute('protocolEquipment');
      }
      if (ctx.instance.commitMsg) {
        ctx.hookState.commitMsg = ctx.instance.commitMsg;
        ctx.instance.unsetAttribute('commitMsg');
      }
    }
    else if (ctx.data) {

      ctx.data.modified = Date.now();

      if (ctx.data.protocolSteps) {
        ctx.hookState.steps = ctx.data.protocolSteps;
        delete ctx.data.protocolSteps;
      }
      if (ctx.data.protocolTags) {
        ctx.hookState.tags = ctx.data.protocolTags;
        delete ctx.data.protocolTags;
      }
      if (ctx.data.protocolMaterials) {
        ctx.hookState.materials = ctx.data.protocolMaterials;
        delete ctx.data.protocolMaterials;
      }
      if (ctx.data.protocolEquipment) {
        ctx.hookState.equipment = ctx.data.protocolEquipment;
        delete ctx.data.protocolEquipment;
      }
      if (ctx.data.commitMsg) {
        ctx.hookState.commitMsg = ctx.data.commitMsg;
        delete ctx.data.commitMsg;
      }
    }
    next();
  });

  Protocol.observe('after save', function addTagsAndSteps(ctx, next){

    var Step = Protocol.app.models.Step;
    var Tag = Protocol.app.models.Tag;
    var Material = Protocol.app.models.Material;
    var Equipment = Protocol.app.models.Equipment;

    var protocolRes = ctx.instance;

    var tags = ctx.hookState.tags ? ctx.hookState.tags : [];
    var materials = ctx.hookState.materials ? ctx.hookState.materials : [];
    var equipment = ctx.hookState.equipment ? ctx.hookState.equipment : [];
    var steps = ctx.hookState.steps ? ctx.hookState.steps : [];

    // Add tags to the protocol
    // Remove old tags that are not in the new tags list

    protocolRes.tags(function(err, oldTags){

      var newTagsIds = tags.map(function(obj){return obj.id;});

      async.each(oldTags,function(oldTag, callback){
        if (oldTag.id){
          if (newTagsIds.indexOf(oldTag.id.toString()) < 0) {
            protocolRes.tags.remove(oldTag.id, function(err, data){
              callback();
            });
          }
        }
        else {
          callback();
        }

      });

      // Add new tags
      // (Was going to do this only for new tags not in the old list, but had some problems with it... but this works)
      async.each(tags,function(tag, callback){
        if (tag.id) {
          protocolRes.tags.add(tag.id, function(err, val){
            if (err) {
              debug('Protocol tag add error 1: ' + String(err));
            }
            callback();
          });
        }
        else {
          // Check if the tag already exists
          Tag.find({where: {text: tag.text}}, function(err, val){
            debug('tag without id, lookup result: ', val);

            if (err) {
              debug('Error looking for preexisting tag: ' + String(err));
            }

            if (val.length > 0) {
              protocolRes.tags.add(val[0].id, function(err, val){
                if (err) {debug('Protocol tag add error 2: ' + String(err));}
                callback();
              });
            }
            else {
              protocolRes.tags.create(tag, function(err, val){
                if (err) {debug('Protocol tag create error: ' + String(err));}
                callback();
              });
            }
          });
        }
      });

    });

    // Remove old materials that are not in the new materials list
    protocolRes.materials(function(err, oldMaterials){

      var newMaterialsIds = materials.map(function(obj){return obj.id;});

      async.each(oldMaterials,function(oldMaterial, callback){
        if (oldMaterial.id){
          if (newMaterialsIds.indexOf(oldMaterial.id.toString()) < 0) {
            protocolRes.materials.remove(oldMaterial.id, function(err, data){
              callback();
            });
          }
        }
        else {
          callback();
        }
      });

      // Add new materials
      // (Was going to do this only for new materials not in the old list, but had some problems with it... but this works)
      async.each(materials,function(material, callback){
        if (material.id) {
          protocolRes.materials.add(material.id, function(err, val){
            if (err) {
              debug('Protocol material add error 1: ' + String(err));
              callback();
            }
          });
        }
        else {
          //Check if the tag already exists
          Material.find({where: {name: material.name}}, function(err, val){
            debug('material without id, lookup result: ',val);

            if (err) {
              debug('Error looking for preexisting material: ' + String(err));
            }

            if (val.length > 0) {
              protocolRes.materials.add(val[0].id, function(err, val){
                if (err) {
                  debug('Protocol material add error 2: ' + String(err));
                }
                callback();
              });
            }
            else {
              protocolRes.materials.create(material, function(err, val){
                if (err) {
                  debug('Protocol material create error: ' + String(err));
                }
                callback();
              });
            }
          });
        }
      });

    });

    // Remove old equipment that are not in the new equipment list
    protocolRes.equipment(function(err, oldEquipment){

      var newEquipmentIds = equipment.map(function(obj){return obj.id;});

      async.each(oldEquipment,function(oldEquipmenti, callback){
        if (oldEquipmenti.id){
          if (newEquipmentIds.indexOf(oldEquipmenti.id.toString()) < 0) {
            protocolRes.equipment.remove(oldEquipmenti.id, function(err, data){
              callback();
            });
          }
        }
        else {
          callback();
        }

      });

      // Add new equipment
      // (Was going to do this only for new equipments not in the old list, but had some problems with it... but this works)
      async.each(equipment,function(equi, callback){
        if (equi.id) {
          protocolRes.equipment.add(equi.id, function(err, val){
            if (err) {
              debug('Protocol equipment add error 1: ' + String(err));
            }
            callback();
          });
        }
        else {
          // Check if the tag already exists
          Equipment.find({where: {name: equi.name}}, function(err, val){
            debug('equipment without id, lookup result: ',val);

            if (err) {
              debug('Error looking for preexisting equipment: ' + String(err));
            }

            if (val.length > 0) {
              protocolRes.equipment.add(val[0].id, function(err, val){
                if (err) {
                  debug('Protocol equipment add error 2: ' + String(err));
                }
                callback();
              });
            }
            else {
              protocolRes.equipment.create(equi, function(err, val){
                if (err) {
                  debug('Protocol equipment create error: ' + String(err));
                }
                callback();
              });
            }
          });
        }
      });

    });

    // Make sure steps belong to the right protocol
    steps = steps.map(function(obj){
      obj.protocolId = protocolRes.id;
      return obj;
    });

    // First delete all steps of the protocol (upserting many steps at a time causes a Mongo error)
    protocolRes.steps.destroyAll(function(err2, response){

      // if(err2){ cb(err2, null, null);}

      // Then insert the updated steps in the database
      Step.upsert(steps, function(err3, stepsRes){
      // if(err3){ cb(err3, null, null);}
      // Could add the steps to the protocol response here if necessary
      ctx.hookState.steps = stepsRes;
      next();
    });
    });
  });

  Protocol.observe('after save', function generateCoverphoto(ctx, next){

    debug('after creating hook');
    if (ctx.instance.id && ctx.isNewInstance) {

      var Trianglify = require('trianglify');
      var pattern = Trianglify({width: 600, height: 300,
        seed: ctx.instance.id}).png();
      var pic = pattern.substr(pattern.indexOf('base64') + 7);
      var buffer = new Buffer(pic, 'base64');

      // Unfortunately we have to go with direct S3 access
      var AWS = require('aws-sdk');
      var AWSconfig = require('../../server/datasources.json');
      AWS.config.update({accessKeyId: AWSconfig.amazonS3.keyId,
        secretAccessKey: AWSconfig.amazonS3.key});
      var s3Bucket = new AWS.S3({
        params: {Bucket: 'scitocols-protocol-coverphotos'}});

      var data = {
        Key: ctx.instance.id + '.jpg',
        Body: buffer,
        ContentEncoding: 'base64',
        ContentType: 'image/jpeg'
      };
      s3Bucket.putObject(data, function(err, data){
        if (err) {
          debug(err);
          debug('Error uploading data: ', data);
        }
        else {
          debug('succesfully uploaded the image!');
        }
      });

      ctx.instance.status = 'created';
      ctx.instance.created = Date.now();
    }
    next();
  });

  Protocol.observe('after save', function doGitStuff(ctx, next){
    debug('In remote hook after upserting. Let us do some git!');

    // var protocol = modelInstance.protocol;
    var protocol = ctx.instance;
    // var steps = modelInstance.steps;
    var steps = ctx.hookState.steps;

    debug('Are we creating a new protocol?: ' + ctx.isNewInstance);

    // Get information about the current user logged in (for creating git commits later)
    var currCtx = loopback.getCurrentContext();
    var currentUser = currCtx && currCtx.get('currentUser');

    if (currentUser) {
      var currentUserId = currentUser.id;
      var currentUserEmail = currentUser.email;
      var currentUserFullName = currentUser.firstName + ' ' +
        currentUser.lastName;
    }
    else {
      var currentUserId = '0';
      var currentUserEmail = 'admin@admin.com';
      var currentUserFullName = 'Admin User';
    }

    var commitMsg = '' + ctx.hookState.commitMsg;

    debug('Saved protocol with id %s, named %s to database. git msg %s.',
      protocol.id, protocol.name, commitMsg);

    // Store date for git signatures
    var date = new Date();
    var timeStamp = date.getTime();
    timeStamp = timeStamp / 1000;

    // Directory for storing the git repo (still local)
    var repoDir = '../../storage/repos/' + protocol.id;
    var fileName = 'protocol.md';
    var fileContent = protocol.description;

    var repository;
    var index;
    var oid;

    if (ctx.isNewInstance) {
      /**
           * Here, we create a new file, 'protocol.md', add it to the git
           * index and commits it to head. Similar to a 'git add protocol.md',

           **/

      debug('This is a new instance. Creating a git repo.');

      // Open the directory for git repo
      fse.ensureDirAsync(path.resolve(__dirname, repoDir))
    .then(function(){
      // git init
      return nodegit.Repository.init(path.resolve(__dirname, repoDir), 0);
    })
    .then(function(repo){
      // Write to the actual file
      repository = repo;
      // This could be done with Promise.all, see index-add-remove nodegit example
      return fse.writeFileAsync(path.join(repository.workdir(), fileName),
        fileContent);
    })
  .then(function(repo){
    // Also write useful data about the protocol
    return fse.writeFileAsync(path.join(repository.workdir(), 'protocol.info'),
      JSON.stringify(protocol));
  })
	.then(function(){
  // Write the steps
  return Promise.all(steps.map(function(step){
    fse.writeFileAsync(
      path.join(repository.workdir(), step.id + '.md'), step.text);
    fse.writeFileAsync(
      path.join(repository.workdir(), step.id + '.info'), JSON.stringify(step));
  }));
	})
	.then(function(){
  // Open index
  return repository.openIndex();
})
    .then(function(idx){
     index = idx;
     return index.read(1);
   })
    .then(function(){
      // As this is the initial commit, we can simply add all the files.
      return index.addAll();
    })
    .then(function(){
     return index.write();
   })
    .then(function(){
     return index.writeTree();
   })
    .then(function(oidResult){
      oid = oidResult;
      // Create a git commit message
      var author = nodegit.Signature.create(currentUserFullName,
        currentUserEmail, timeStamp, date.getTimezoneOffset());
      var committer = nodegit.Signature.create(currentUserFullName,
        currentUserEmail, timeStamp, date.getTimezoneOffset());

      // Since we're creating an inital commit, it has no parents. Note that unlike
      // normal we don't get the head either, because there isn't one yet.
      return repository.createCommit('HEAD', author, committer, commitMsg,
        oid, []);

    })
	.done(function(commitId){
  console.log('New Commit: ', commitId);
  protocol.commits.create(
    {msg: commitMsg,
     userId: currentUserId,
     oid: commitId},
      function(err, commit){
        if (err) {
          debug(err);
        }
        console.log('Successfully created a commit object on server.');
      });
	});
    }
    else {
      /**
           * Here, we edit the file, 'protocol.md', add it to the git
           * index and commits it to head. Similar to a 'git add protocol.md',
           * followed by a 'git commit'.
           **/

      debug('Already existing protocol. Only updating git repo');

      // Open the git repository
      nodegit.Repository.open(path.resolve(__dirname, repoDir + '/.git'))
    .then(function(repoResult){
      repository = repoResult;
      return fse.ensureDirAsync(path.join(repository.workdir()));
    }).then(function(){
      // Update the file in the repo
      return fse.writeFileAsync(path.join(repository.workdir(), fileName),
        fileContent);
    })
    .then(function(repo){
      // Also write useful data about the protocol
      return fse.writeFileAsync(path.join(repository.workdir(),
        'protocol.info'), JSON.stringify(protocol));
    })
    .then(function(){
      // Write the steps
      return Promise.all(steps.map(function(step){
        fse.writeFileAsync(
          path.join(repository.workdir(), step.id + '.md'), step.text);
        fse.writeFileAsync(
          path.join(repository.workdir(), step.id + '.info'),
            JSON.stringify(step));
      }));
    })
   .then(function(){
     // Open the git index for updating it
     return repository.openIndex();
   })
    .then(function(indexResult){
      index = indexResult;
      return index.read(1);
    })
    .then(function(){
      // Add all the changes!
      return index.addAll();
    })
    .then(function(){
        // this will write all files to the index.
        return index.write();
      })
    .then(function(){
      return index.writeTree();
    })
    .then(function(oidResult){
      oid = oidResult;
      return nodegit.Reference.nameToId(repository, 'HEAD');
    })
    .then(function(head){
      // Get current HEAD commit (which will be the parent of new commit)
      return repository.getCommit(head);
    })
    .then(function(parent){
      // Create git signature
      var author = nodegit.Signature.create(currentUserFullName,
        currentUserEmail, timeStamp, date.getTimezoneOffset());
      var committer = nodegit.Signature.create(currentUserFullName,
        currentUserEmail, timeStamp, date.getTimezoneOffset());

      // New commit
      return repository.createCommit('HEAD', author, committer, commitMsg,
        oid, [parent]);
    })
    .catch(function(reasonForFailure){
      debug('Failed to update git repo');
      // failure is handled here
    })
    .done(function(commitId){
      console.log('New Commit: ', commitId);
      protocol.commits.create(
        {msg: commitMsg,
         userId: currentUserId,
         oid: commitId},
        function(err, commit){
          if (err) {
            debug(err);
          }
        });
    });
    }
    next();
  });

  Protocol.getVersion = function(protocolId, oid, cb){
    repoDir = '../../storage/repos/' + protocolId + '/.git';
    gitLocation = path.resolve(__dirname, repoDir);
    data = {repoLocation: gitLocation, oid: oid};
    scitogit.gitGetVersion(data, function(gitdiff){
      cb(null, gitdiff);
    });
  };

  Protocol.remoteMethod(
      'getVersion',
      {
        accepts: [
          {arg: 'protocolId', type: 'string'},
          {arg: 'oid', type: 'string'}
        ],
        returns: {arg: 'protocol', type: 'Object'}
      }
  );

  Protocol.findPublic = function(filter, cb){

    // Create empty filter if there is none provided
    if (!(filter)){
      filter = {};
    }
    debug('filter: ', filter);

    //This is what we want to add to the filter
    addFilter = {where: {isPublic: true}};
    _.deepExtend(filter,addFilter);
    debug('findPublic modified filter: ', filter);

    Protocol.find(filter)
    .then(function(protocols){
      // Should here make absolutely sure there are no private protocols
      cb(null, protocols);
    })
    .catch(function(error){
      cb(error, null);
    });

  };

  Protocol.remoteMethod('findPublic',
  {
    accepts: {arg: 'filter', type: 'object'},
    returns: {arg: 'protocols', type: '[object]'}
  });

  Protocol.countPublic = function(filter, cb){

    // Create empty filter if there is none provided
    if (!(filter)){
      filter = {};
    }
    debug('filter: ', filter);

    // This is what we want to add to the filter
    addFilter = {where: {isPublic: true}};
    _.deepExtend(filter, addFilter);
    debug('modified filter: ', filter);

    Protocol.count(filter.where)
    .then(function(count){
      // Should here make absolutely sure there are no private protocols
      cb(null, count);
    })
    .catch(function(error){
      cb(error, null);
    });

  };

  Protocol.remoteMethod('countPublic',
  {
    accepts: {arg: 'filter', type: 'object'},
    returns: {arg: 'count', type: 'number'}
  });

  Protocol.countInMine = function(filter, cb){

    var User = Protocol.app.models.user;
    var currCtx = loopback.getCurrentContext();
    var currentUser = currCtx && currCtx.get('currentUser');

    if (currentUser){
      Protocol.inMineFilter(filter)
      .then(function(newFilter){
        debug('newFilter: ', JSON.stringify(newFilter));
        return Protocol.count(newFilter.where);
      })
      .then(function(count){
        // Should check that only the right protocols are here
        cb(null, count);
      })
      .catch(function(error){
        cb(error, null);
      });

    }
    else {
      cb(new Error('It seems user is not logged in, ' +
        'although ACL should catch that.'), null);
    }
  };

  Protocol.remoteMethod('countInMine',
  {
    accepts: {arg: 'filter', type: 'object'},
    returns: {arg: 'count', type: 'number'}
  });

  Protocol.findInMine = function(filter, cb){
    var User = Protocol.app.models.user;
    var currCtx = loopback.getCurrentContext();
    var currentUser = currCtx && currCtx.get('currentUser');

    if (currentUser){
      Protocol.inMineFilter(filter)
      .then(function(newFilter){
        debug('findInMine newFilter: ', JSON.stringify(newFilter));
        return Protocol.find(newFilter);
      })
      .then(function(protocols){
        // Should check that only the right protocols are here
        cb(null, protocols);
      })
      .catch(function(error){
        cb(error, null);
      });

    }
    else {
      cb(new Error('It seems user is not logged in, ' +
        'although ACL should catch that.'), null);
    }
  };

  Protocol.remoteMethod('findInMine',
  {
    accepts: {arg: 'filter', type: 'object'},
    returns: {arg: 'protocols', type: '[object]'}
  });

  Protocol.inMineFilter = function(filter){

    var inFilter = filter;

    var currCtx = loopback.getCurrentContext();
    var currentUser = currCtx && currCtx.get('currentUser');
    var User = Protocol.app.models.user;

    return User.findById(currentUser.id, {include: ['joinedTeam', 'ownedTeam']})
    .then(function(response){
      //Because 'filter' inexplicably gets deleted...
      var filter = inFilter;

      var userData = response.toJSON();
      var teamIds = userData.joinedTeam.map(function(obj){
        return String(obj.id);});
      teamIds = teamIds.concat(userData.ownedTeam.map(function(obj){
        return String(obj.id);}));

      if (!(filter)){
        var filter = {where:  {}};
      }

      var addFilter =  {or: [
        {ownerType: 'user', ownerId: String(currentUser.id)},
        {ownerType: 'team', ownerId: {inq: teamIds}}
        ]};

      var newFilter = {where: {and: []}};

      newFilter.where.and[0] = _.deepExtend({}, filter.where);
      newFilter.where.and[1] = _.deepExtend({}, addFilter);

      //Add other query fields
      if (filter.include){
        newFilter.include = filter.include;
      }
      if (filter.skip){
        newFilter.skip = filter.skip;
      }
      if (filter.limit){
        newFilter.limit = filter.limit;
      }
      if (filter.fields){
        newFilter.fields = filter.fields;
      }
      if (filter.order){
        newFilter.order = filter.order;
      }
      if (filter.offset){
        newFilter.offset = filter.offset;
      }

      return newFilter;

    });
  };

  Protocol.afterRemote('findById',
    function checkIfAuthorisedToView(ctx, instance, next){

      var User = Protocol.app.models.user;

      if (instance.isPublic){
        next();
      }
      else {

        if (!(ctx.req.accessToken)){
          next(new Error('Unauthenticated user trying to ' +
            'view private protocol.'));
        }
        else {
          var userId = ctx.req.accessToken.userId;
          if (String(instance.ownerType) == 'user'){
            if (String(instance.ownerId) == String(userId)){
              next();
            }
            else {
              next(new Error('User is trying to access another ' +
                'user\'s private protocol.'));
            }
          }
          else if (String(instance.ownerType) == 'team'){
            User.findById(userId, {include: ['joinedTeam', 'ownedTeam']})
            .then(function(response){

              var userData = response.toJSON();
              var teamIds = userData.joinedTeam.map(function(obj){
                return String(obj.id);});
              teamIds = teamIds.concat(userData.ownedTeam.map(function(obj){
                return String(obj.id);}));

              if (teamIds.indexOf(String(instance.ownerId)) > -1){
                next();
              }
              else {
                next(new Error('User is trying to access a private protocol ' +
                  'of a team he/she does not belong to'));
              }

            });

          }
          else {
            next(new Error('Protocol ownership is undefined.'));
          }
        }

      }
    }
  );

  //Helper for searching by model related to protocols
  Protocol.searchPublicByHelper = function(ids, model){

    var modelPromises = [];

    var findParams = {
        include: {
          relation: 'protocols',
          scope: {where: {isPublic: true}}
        }
      };

    ids.forEach(function(id, index, array){
      modelPromises[index] = model.findById(id, findParams)
      .then(function(response){
        return response.toJSON().protocols;
      });
    });

    return Promise.all(modelPromises)
    .then(function(response){

      debug('all: ',response);
      debug('intersection: ', intersection.intersectionObjects(response));
      return intersection.intersectionObjects(response);

    })
    .catch(function(error){
      return error;
    });

  };

  //Takes an array of tag ids and returns protocols matching all
  Protocol.searchPublicByTags = function(tagIds){

    var Tag = Protocol.app.models.Tag;

    return Protocol.searchPublicByHelper(tagIds, Tag);
    
  };

  Protocol.remoteMethod('searchPublicByTags',
  {
    accepts: {arg: 'tagIds', type: '[string]'},
    returns: {arg: 'protocols', type: '[object]'}
  });

  //Takes an array of material ids and returns protocols matching all
  Protocol.searchPublicByMaterials = function(materialIds){

    var Material = Protocol.app.models.Material;

    return Protocol.searchPublicByHelper(materialIds, Material);
    
  };

  Protocol.remoteMethod('searchPublicByMaterials',
  {
    accepts: {arg: 'materialIds', type: '[string]'},
    returns: {arg: 'protocols', type: '[object]'}
  });

  //Takes an array of equipment ids and returns protocols matching all
  Protocol.searchPublicByEquipment = function(equipmentIds){

    var Equipment = Protocol.app.models.Equipment;

    return Protocol.searchPublicByHelper(equipmentIds, Equipment);
    
  };

  Protocol.remoteMethod('searchPublicByEquipment',
  {
    accepts: {arg: 'equipmentIds', type: '[string]'},
    returns: {arg: 'protocols', type: '[object]'}
  });

  //Helper function for searchInMine by related model ids
  Protocol.searchInMineByHelper = function(ids, model){

    var modelPromises = [];

    return Protocol.inMineFilter()
    .then(function(filter){

      var findParams = {
          include: {
            relation: 'protocols'
          }
        };
      findParams.include.scope = filter;

      console.log(JSON.stringify(findParams));

      ids.forEach(function(id, index, array){

        modelPromises[index] = model.findById(id, findParams)
        .then(function(response){
          return response.toJSON().protocols;
        });
      });

      return Promise.all(modelPromises);
    })
    .then(function(response){

      debug('all: ',response);
      debug('intersection: ', intersection.intersectionObjects(response));
      return intersection.intersectionObjects(response);

    })
    .catch(function(error){
      return error;
    });

  };

  //Takes an array of tag ids and returns protocols matching all
  Protocol.searchInMineByTags = function(tagIds){

    var Tag = Protocol.app.models.Tag;

    return Protocol.searchInMineByHelper(tagIds, Tag);

  };

  Protocol.remoteMethod('searchInMineByTags',
  {
    accepts: {arg: 'tagIds', type: '[string]'},
    returns: {arg: 'protocols', type: '[object]'}
  });

  //Takes an array of tag ids and returns protocols matching all
  Protocol.searchInMineByMaterials = function(materialIds){

    var Material = Protocol.app.models.Material;

    return Protocol.searchInMineByHelper(materialIds, Material);

  };

  Protocol.remoteMethod('searchInMineByMaterials',
  {
    accepts: {arg: 'materialIds', type: '[string]'},
    returns: {arg: 'protocols', type: '[object]'}
  });

  //Takes an array of tag ids and returns protocols matching all
  Protocol.searchInMineByEquipment = function(equipmentIds){

    var Equipment = Protocol.app.models.Equipment;

    return Protocol.searchInMineByHelper(equipmentIds, Equipment);

  };

  Protocol.remoteMethod('searchInMineByEquipment',
  {
    accepts: {arg: 'equipmentIds', type: '[string]'},
    returns: {arg: 'protocols', type: '[object]'}
  });

};

