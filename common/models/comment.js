module.exports = function(Comment){

  Comment.observe('before save', function setCreatedDate(ctx, next){
    if (ctx.instance) {
      ctx.instance.created = Date.now();
    }
    next();
  });
};
