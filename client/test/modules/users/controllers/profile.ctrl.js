'use strict';

describe('Controller: ProfileCtrl', function () {
  var ProfileCtrl,
    scope;

  // load the controller's module
  beforeEach(module('ngRoute'))
  beforeEach(module('ngCookies'));
  beforeEach(module('ui.router'));
  beforeEach(module('gettext'));
  beforeEach(module('config'));
  beforeEach(module('toasty'));
  beforeEach(module('oitozero.ngSweetAlert'));
  beforeEach(module('formly'));
  beforeEach(module('angularFileUpload'));
  beforeEach(module('angular-loading-bar'));
  beforeEach(module('lbServices'));
  beforeEach(module('com.module.core'));
  beforeEach(module('com.module.users'));

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $timeout, CoreService, User, gettextCatalog, FileUploader) {
    scope = $rootScope.$new();
    ProfileCtrl = $controller('ProfileCtrl', {
      $scope: scope,
      $timeout: $timeout,
      CoreService: CoreService,
      User: User,
      gettextCatalog: gettextCatalog,
      Upload: FileUploader
    });
  }));

  it('should have a submit function attached to scope', function () {
    expect(scope.onSubmit).toBeDefined();
  });

});