'use strict';

describe('Controller: com.module.core.users:LoginCtrl', function () {
  var LoginCtrl,
    scope;

  // load the controller's module
  beforeEach(module('ngRoute'))
  beforeEach(module('ngCookies'));
  beforeEach(module('ui.router'));
  beforeEach(module('gettext'));
  beforeEach(module('config'));
  beforeEach(module('toasty'));
  beforeEach(module('oitozero.ngSweetAlert'));
  beforeEach(module('formly'));
  beforeEach(module('angular-loading-bar'));
  beforeEach(module('lbServices'));
  beforeEach(module('com.module.core'));
  beforeEach(module('com.module.users'));

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, CoreService, User, gettextCatalog) {
    scope = $rootScope.$new();
    LoginCtrl = $controller('LoginCtrl', {
      $scope: scope,
      User: User,
      gettextCatalog: gettextCatalog
    });
  }));

  it('should remember a user for two weeks', function () {
    var TWO_WEEKS = 1000 * 60 * 60 * 24 * 7 * 2;
    expect(scope.credentials.ttl).toEqual(TWO_WEEKS);
    expect(scope.credentials.rememberMe).toEqual(true);
  });

});