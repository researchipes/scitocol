'use strict';

describe('Controller: com.module.core.controller:ProtocolsCtrl', function () {
  var ProtocolsCtrl,
    scope;

  // load the controller's module
  beforeEach(module('ui.router'));
  beforeEach(module('config'));
  beforeEach(module('formly'));
  beforeEach(module('angular-loading-bar'));
  beforeEach(module('lbServices'));
  beforeEach(module('com.module.core'));
  beforeEach(module('com.module.protocols'));
  beforeEach(module('com.module.tags'));


  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $stateParams, 
    Protocol, Tag) {
    scope = $rootScope.$new();
    $stateParams.tags = [];
    ProtocolsCtrl = $controller('ProtocolListCtrl', {
      $scope: scope,
      $stateParams, $stateParams,
      Protocol: Protocol,
      Tag: Tag
    });
  }));

  it('should be loading in the beginning', function () {
    expect(scope.loading).toBeTruthy();
  });
});