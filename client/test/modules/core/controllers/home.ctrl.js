'use strict';

describe('Controller: com.module.core.controller:HomeCtrl', function () {
  var HomeCtrl,
    scope, $rootScope;

  // load the controller's module
  beforeEach(module('ngCookies'));
  beforeEach(module('ui.router'));
  beforeEach(module('gettext'));
  beforeEach(module('config'));
  beforeEach(module('toasty'));
  beforeEach(module('oitozero.ngSweetAlert'));
  beforeEach(module('formly'));
  beforeEach(module('angular-loading-bar'));
  beforeEach(module('lbServices'));
  beforeEach(module('com.module.core'));
  beforeEach(module('com.module.protocols'));


  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, _$rootScope_) {
    $rootScope = _$rootScope_;
    scope = $rootScope.$new();
    HomeCtrl = $controller('HomeCtrl', {
      $scope: scope,
      $rootScope: $rootScope
    });
  }));

  it('should have empty count in the beginning', function () {
    expect(scope.count).toEqual({});
  });

  it('should have the same boxes as rootScope', function(){
    expect(scope.boxes).toEqual($rootScope.dashboardBox);
  })
});