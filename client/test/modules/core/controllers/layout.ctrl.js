'use strict';

describe('Controller: com.module.core.controller:LayoutCtrl', function () {
  var LayoutCtrl,
    scope, cookies;

  // load the controller's module
  beforeEach(module('ngCookies'));
  beforeEach(module('ui.router'));
  beforeEach(module('gettext'));
  beforeEach(module('config'));
  beforeEach(module('toasty'));
  beforeEach(module('oitozero.ngSweetAlert'));
  beforeEach(module('formly'));
  beforeEach(module('angular-loading-bar'));
  beforeEach(module('lbServices'));
  beforeEach(module('com.module.core'));
  beforeEach(module('com.module.settings'));


  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $cookies, CoreService) {
    scope = $rootScope.$new();
    LayoutCtrl = $controller('LayoutCtrl', {
      $scope: scope, $cookies: $cookies, CoreService: CoreService
    });
  }));

  it('appname should be Scitocol', function () {
    expect(scope.appName).toMatch('Scitocol');
  });

});