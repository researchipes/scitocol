"use strict";

exports.config = {
    allScriptsTimeout: 11000,

    seleniumAddress: 'http://localhost:4444/wd/hub',

    specs: [
        "e2e/*.js"
    ],

    multiCapabilities: [{
        browserName: "chrome"
    }],

    baseUrl: "http://localhost:9000/",

    beforeLaunch: function() {
        console.log("Starting setup...");

        // Return a promise when dealing with asynchronous
        // functions here (like creating users in the database)
    },

    afterLaunch: function() {
        console.log("Starting cleanup...");

        // Return a promise when dealing with asynchronous
        // functions here (like removing users from the database)
    },

    framework: "jasmine",

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    }
};