"use strict";

describe("Login tester", function() {
    beforeEach(function() {
        // get the root of the project
        browser.get("/#");
    });

  // it('should have a title', function() {
  //   expect(browser.getTitle()).toEqual('Admin: Loopback Admin');
  // });

  // // test default route
  // it('should jump to the /login path when /# is accessed', function() {
  //   expect(browser.getLocationAbsUrl()).toMatch("/login");
  // });

  it('ensures users can login and create a protocol', function() {
    // For some reason, this is not working still...
    // browser.get('/#/login');
    // This is useful for writing to angular variables
    // element(by.model('credentials.email')).clear();
    // element(by.model('credentials.email')).sendKeys('All your base are belong to us');
    element(by.id('signinButton')).click();
    expect(browser.getLocationAbsUrl()).toMatch("/app");
    element(by.id('menuoptionProtocols')).click();
    expect(browser.getLocationAbsUrl()).toMatch("/app/protocols");
    element(by.id('buttonProtocolAdd')).click();
    expect(browser.getLocationAbsUrl()).toMatch("/app/protocols/add");
    element(by.model('protocol.title')).clear();
    element(by.model('protocol.title')).sendKeys('All your base are belong to us');
    element(by.id('buttonProtocolSave')).click();
    expect(browser.getLocationAbsUrl()).toMatch("/app/protocols");
  });


});