"use strict";

describe("Login tester", function() {
    beforeEach(function() {
        // get the root of the project
        browser.get("/#");
    });

  it('should have a title', function() {
    expect(browser.getTitle()).toEqual('scitocols');
  });

  // test default route
  it('should jump to the /login path when /# is accessed', function() {
    expect(browser.getLocationAbsUrl()).toMatch("/login");
  });

  it('ensures users can login', function() {
    // For some reason, this is not working still...
    // browser.get('/#/login');
    element(by.id('signinButton')).click();
    expect(browser.getLocationAbsUrl()).toMatch("/app");
  });


});