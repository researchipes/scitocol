'use strict';
/*jshint unused: vars */
/**
 * @ngdoc directive
 * @name com.module.comments.directive:commentEditor
 * @description
 * # commentEditor
 */
angular.module('com.module.comments')
  .directive('commentEditor', ['Comment', 'CommentsService', 'CoreService',
      'LoopBackAuth', 'User',
      function(Comment, CommentsService, CoreService, LoopBackAuth, User){
    return {
      templateUrl: 'modules/comments/directives/commentEditorTemplate.html',
      scope: {
        commentableId: '@',
        commentsArray: '='
      },
      link: function($scope, $element, $attrs){
        $scope.comment = {
          text: '',
          commentableType: $scope.ownerType,
          commentableId: $scope.commentableId,
          userId: LoopBackAuth.currentUserId
        };

        $scope.addCommentLocal = function(){
          Comment.upsert($scope.comment).$promise
          .then(
            function(data){

              CoreService.toastSuccess('Comment submitted!');

              CommentsService.loadComments($scope.commentableId)
              .then(function(thecomments){
                $scope.commentsArray = thecomments;
              }, function(error){
                console.log(error);
              });

              $scope.comment.text = '';

            },
            function(err){
              console.log(err);
            });
        };
      }
    };
  }]);
