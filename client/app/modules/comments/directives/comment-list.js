'use strict';
/*jshint unused: vars */
/**
 * @ngdoc directive
 * @name com.module.comments.directive:commentList
 * @description
 * # commentList
 */
angular.module('com.module.comments')
  .directive('commentList', ['CommentsService',function(CommentsService){
    return {
      templateUrl: 'modules/comments/directives/commentListTemplate.html',
      scope: {
        commentableId: '@',
        commentsArray: '@',
        isDiscussion: '@'
      },
      link: function(scope, element, attrs){

        scope.$watch('commentsArray',function(newValue, oldValue){
          CommentsService.loadComments(scope.commentableId)
          .then(function(thecomments){
            scope.commentsArray = thecomments;
          }, function(error){
            console.log(error);
          });
        });

        function init(){
          CommentsService.loadComments(scope.commentableId)
          .then(function(thecomments){
            scope.commentsArray = thecomments;
          }, function(error){
            console.log(error);
          });
        }

        init();

      }
    };
  }]);
