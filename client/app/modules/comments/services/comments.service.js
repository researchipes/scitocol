'use strict';
var app = angular.module('com.module.comments');

app.service('CommentsService',['Comment', 'CoreService', function(Comment,
    CoreService){

  this.deleteComment = function(id, cb){
    CoreService.confirm('Are you sure?',
    'Deleting this cannot be undone',
      function(){
        Comment.deleteById(id, function(){
    CoreService.toastSuccess(
    'Comment deleted',
            'Your protocol is deleted!');

    cb();
    // loadComments();
    // $state.go('app.protocols.list');
  }, function(err){
    CoreService.toastError(
    'Error deleting protocol',
            'Your protocol is not deleted: ' + err);
  });
      },
      function(){
        return false;
      });
  };

  this.loadComments = function(theid){
    return Comment.find(
      {filter: {where: {'commentableId': theid},
                include: ['user']}}).$promise
    .then(function(response){
      return response;
    },
      function(error){
        console.log(error);
      });
  };

}]);
