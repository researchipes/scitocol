'use strict';
angular.module('com.module.comments')
.controller('CommentsCtrl', function($scope, CommentsService, Comment){

  $scope.loading = true;

  $scope.status = {
    isopen: false
  };

  function loadComments(){
    $scope.comments = Comment.find(function(){
      console.log('after find');
      $scope.loading = false;
    });
  }

  loadComments();

  $scope.delete = function(id){
    CommentsService.deleteComment(id, loadComments);
  };

});
