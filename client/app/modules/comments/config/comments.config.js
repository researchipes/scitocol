'use strict';
angular.module('com.module.comments')
  .run(function($rootScope, Comment){

    Comment.count(function(data){
      $rootScope.addDashboardBox('Comments',
        'bg-blue', 'ion-chatbubbles', data.count, 'app.comments.list');
    });

  });
