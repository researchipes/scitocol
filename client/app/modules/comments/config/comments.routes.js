'use strict';
angular.module('com.module.comments')
  .config(function($stateProvider){
    $stateProvider.state('app.comments', {
        abstract: true,
        url: '/comments',
        templateUrl: 'modules/comments/views/main.html'
      })
      .state('app.comments.list', {
        url: '',
        templateUrl: 'modules/comments/views/list.html',
        controller: 'CommentsCtrl'
      });
  });
