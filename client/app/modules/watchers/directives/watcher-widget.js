'use strict';
/*jshint unused: vars */
/**
 * @ngdoc directive
 * @name com.module.comments.directive:commentEditor
 * @description
 * # commentEditor
 */
angular.module('com.module.watchers')
  .directive('watcherWidget', ['Watcher', 'WatchersService', '$stateParams',
  'CoreService','LoopBackAuth','User', function(Watcher, WatchersService,
  $stateParams, CoreService, LoopBackAuth, User){

    return {
      templateUrl: 'modules/watchers/directives/watcherWidgetTemplate.html',
      scope: {
        watchercount: '=',
        watching: '='
      },

      link: function($scope, $element, $attrs){

        $scope.$watch('watchercount',function(newValue, oldValue){
          $scope.watchercount = newValue;
        });
        $scope.$watch('watching',function(newValue, oldValue){
          $scope.watching = newValue;
        });

        $scope.toggleWatcher = function(){
          WatchersService.toggleWatcher($stateParams.id,
            LoopBackAuth.currentUserId)
          .then(function(res){
            return res;
          })
          .then(function(res2){
            return WatchersService.myWatcher($stateParams.id,
              LoopBackAuth.currentUserId);
          })
          .then(function(res3){
            $scope.watching = res3;
            return WatchersService.countWatchers($stateParams.id);
          })
          .then(function(res4){
            $scope.watchercount = res4;
            return res4;
          })
          .catch(function(error){
            console.log(error);
          });

        };

        function init(){
          $scope.protoId = $stateParams.id;
          WatchersService.myWatcher($stateParams.id,LoopBackAuth.currentUserId)
          .then(function(result){
            $scope.watching = result;
          }, function(error){
            console.log(error);
          });

          WatchersService.countWatchers($stateParams.id).
          then(function(result){
            $scope.watchercount = result;
          }, function(error){
            console.log(error);
          });
        }

        init();

      }
    };
  }]);
