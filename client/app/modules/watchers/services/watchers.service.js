'use strict';
var app = angular.module('com.module.watchers');

app.service('WatchersService',['Watcher', 'CoreService', function(Watcher,
    CoreService){

  this.countWatchers = function(protoId){
    return Watcher.count({where: {'protocolId': protoId}}).$promise
    .then(function(response){
      return response.count;
    })
    .catch(function(error){
      console.log(error);
    });
  };

  this.myWatcher = function(protoId, userId){
    return Watcher.count(
      {where: {'protocolId': protoId,'userId': userId}}).$promise
    .then(function(response){
      return response.count > 0;
    })
    .catch(function(error){
      console.log(error);
    });
  };

  this.toggleWatcher = function(protoId, userId){
    return Watcher.toggleWatcher({protoId: protoId}).$promise
    .then(function(message){
      CoreService.toastSuccess(message.returnMsg);
      return message;
    })
    .catch(function(error){
      console.log(error);
    });
  };

  this.listProtocols = function(userId){
    return Watcher.find({filter:{where:{userId: userId}, include:'protocol'}}).$promise
    .then(function(watchers){
      var protos = watchers.map(function(a) {return a.protocol;});
      var filterprotos = protos.filter(function(e){return e});
      return filterprotos;
    })
    .catch(function(error){
      console.log(error);
    });
  };

  this.listUsers = function(protoId){
    return Watcher.find({filter:{where:{protocolId: protoId}, include:'user'}}).$promise
    .then(function(watchers){
      var users = watchers.map(function(a) {return a.user;});
      var filterusers = users.filter(function(e){return e});
      console.log("users from service: ", users);
      return filterusers;
    })
    .catch(function(error){
      console.log(error);
    });
  };
}]);
