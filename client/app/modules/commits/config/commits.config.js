'use strict';
angular.module('com.module.commits')
  .run(function($rootScope, Commit){
    Commit.count(function(data){
      $rootScope.addDashboardBox('Commits',
        'bg-green', 'ion-compose', data.count, 'app.commits.list');
    });
  });
