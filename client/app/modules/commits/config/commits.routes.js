'use strict';
angular.module('com.module.commits')
  .config(function($stateProvider){
    $stateProvider.state('app.commits', {
        abstract: true,
        url: '/commits',
        templateUrl: 'modules/commits/views/main.html'
      })
      .state('app.commits.list', {
        url: '',
        templateUrl: 'modules/commits/views/list.html',
        controller: 'CommitsCtrl'
      })
      .state('app.commits.view', {
        url: '/:id',
        templateUrl: 'modules/commits/views/view.html',
        controller: 'CommitsCtrl'
      });
  });
