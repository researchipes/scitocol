'use strict';
angular.module('com.module.commits')
  .controller('CommitsCtrl', function($scope, $state, $stateParams, $filter,
    CoreService, gettextCatalog, Commit){

    $scope.loading = true;

    var commitId = $stateParams.id;

    if (commitId) {
      Commit.findById({id: commitId, filter: {include: ['protocol','user']}}).$promise
      .then(function(commit){
        $scope.icommit = commit;
        $scope.commitInfo = Commit.gitShow({commit: commit},
          function(data){},
          function(err){console.log(err);});
        $scope.loading = false;
      })
      .catch(function(err){
        console.log(err);
      });
    }
    else {
      loadCommits();
    }

    function loadCommits(){

      Commit.find().$promise
      .then(function(response){

        $scope.commits = response;
        console.log($scope.commits[0]);
        $scope.loading = false;

      },
      function(error){
        console.log(error);
      });

    }

    $scope.delete = function(id){
      CoreService.confirm(gettextCatalog.getString('Are you sure?'),
        gettextCatalog.getString('Deleting this cannot be undone'),
        function(){
          Commit.deleteById(id, function(){
            CoreService.toastSuccess(gettextCatalog.getString(
              'Commit deleted'), gettextCatalog.getString(
              'Your commit is deleted!'));
            loadCommits();
            $state.go('app.commits.list');
          }, function(err){
            CoreService.toastError(gettextCatalog.getString(
              'Error deleting commit'), gettextCatalog.getString(
              'Your commit is not deleted: ') + err);
          });
        },
        function(){
          return false;
        });
    };
  });
