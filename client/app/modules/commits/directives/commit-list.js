'use strict';
/**
 * @ngdoc directive
 * @name com.module.commits.directive:commitList
 * @description
 * # commitList
 */
angular.module('com.module.commits')
  .directive('commitList', function(){
    return {
      templateUrl: 'modules/commits/directives/commitListTemplate.html',
      scope: {
        commits: '='
      },
    };
  });
