'use strict';
/**
 * @ngdoc directive
 * @name com.module.files.directive:fileUploader
 * @description
 * # fileUploader
 */
angular.module('com.module.files')
  .directive('fileAttachments', function(CoreService, $http, Lightbox){
    return {
      templateUrl: 'modules/files/directives/fileAttachmentsTemplate.html',
      scope: {
        istep: '=',
        files: '=?',
        images: '=?',
        apiUrl: '=?'
      },
      link: function($scope, $element, $attrs){

        function init(){
          $scope.apiUrl = CoreService.env.apiUrl;
          if ($scope.istep !== undefined) {
            $http.get($scope.apiUrl + '/Containers/' +
              $scope.istep + '/files')
            .success(function(data){
                var images = data.filter(function(dat){
                  var ending = dat.name.split('.').pop();
                  if (ending == 'png' || ending == 'jpg' || ending == 'gif' ||
                    ending == 'jpeg' || ending == 'PNG' || ending == 'JPG' ||
                    ending == 'GIF' || ending == 'JPEG'){
                    return dat.name;
                  }
                });

                var files = data.filter(function(dat){
                  var ending = dat.name.split('.').pop();
                  if (ending != 'png' && ending != 'jpg' && ending != 'gif' &&
                    ending != 'jpeg' && ending != 'PNG' && ending != 'JPG' &&
                    ending != 'GIF' && ending != 'JPEG'){
                    return dat.name;
                  }
                });

                images.forEach(function(image){
                  image.url = $scope.apiUrl + 'Containers/' + image.container +
                    '/download/' + image.name;
                });
                $scope.images = images;
                $scope.files = files;
              }
            );
          }
        }

        init();

        $scope.openLightboxModal = function(index){
          Lightbox.openModal($scope.images, index);
        };

      }
    };
  });

