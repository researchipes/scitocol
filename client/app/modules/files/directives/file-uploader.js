'use strict';
/**
 * @ngdoc directive
 * @name com.module.files.directive:fileUploader
 * @description
 * # fileUploader
 */
angular.module('com.module.files')
  .directive('fileUploader', function(){
    return {
      templateUrl: 'modules/files/directives/fileUploaderTemplate.html',
      scope: {
        istep: '=',
        iedit: '=',
        files: '='
      }
    };
  });
