'use strict';
angular.module('com.module.files')
  .controller('FilesCtrl', function($scope, $http, $timeout, CoreService,
  gettextCatalog, Upload, rfc4122){

    $scope.data = {
        file: '',
        progress: '',
        done: ''
      };

    $scope.apiUrl = CoreService.env.apiUrl;
    $scope.load = function(){
      if($scope.istep){
        if ("bucket" in $scope.istep) {
          $http.get(CoreService.env.apiUrl + '/Containers/' +
            $scope.istep.bucket + '/files')
          .success(function(data){
              $scope.files = data;
            }
          );
        }
      }
      
    };

    $scope.load();

    $scope.delete = function(index, id){
      CoreService.confirm(gettextCatalog.getString('Are you sure?'),
        gettextCatalog.getString('Deleting this cannot be undone'),
        function(){
          $http.delete(CoreService.env.apiUrl + '/Containers/' +
            $scope.istep.bucket + '/files/' + encodeURIComponent(id))
          .success(
            function(data, status, headers){
              $scope.files.splice(index, 1);
              CoreService.toastSuccess(gettextCatalog.getString(
                'File deleted'), gettextCatalog.getString(
                'Your file is deleted!'));
            });
        },
        function(){
          return false;
        });
    };

    $scope.submit = function(data){

      var bucketname = '';
      if ($scope.istep.bucket === undefined) {
        var myuuid = rfc4122.v4();
        bucketname = 'scitocols-step-files-' + myuuid;
      }
      else {
        bucketname = $scope.istep.bucket;
      }

      $http.post(CoreService.env.apiUrl + '/Containers/', {'name': bucketname})
      .then(function(data){
        var file = $scope.data.file;

        var S3_BUCKET_URL = CoreService.env.apiUrl + '/Containers/' +
          bucketname + '/upload';

        Upload.upload({
          url: S3_BUCKET_URL,
          file: file,
          fileName: file.name
        }).progress(function(evt){
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          $scope.data.progress = progressPercentage;
        }).success(function(data, status, headers, config){
          //always need to wait a second to get everything ready on the s3!
          $timeout(function(){
            $scope.istep.bucket = bucketname;
            $scope.data.progress = 0;
            $scope.data.file = [];
            $scope.load();
            CoreService.toastSuccess(gettextCatalog.getString(
                'File uploaded'), gettextCatalog.getString(
                'Your file was uploaded!'));

          }, 3000);

        }).error(function(data, status, headers, config){
          console.log('error status: ' + status);
        });
      },function(status){
        console.log('Error status : ' + status);
      });

    };

    $scope.$on('uploadCompleted', function(event){
      $scope.load();
    });

  });
