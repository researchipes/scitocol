'use strict';
var app = angular.module('com.module.tags');

app.service('TagsService',['$q','Tag', 'CoreService',
function($q, Tag, CoreService){

  this.deleteTag = function(tagId){

    var deferred = $q.defer();

    CoreService.confirm('Are you sure?',
      'Deleting this cannot be undone',
          function(){
            Tag.deleteById({id: tagId}).$promise
                .then(function(response){
              CoreService.toastSuccess(
                'Tag deleted',
                'Your tag is deleted!');
              deferred.resolve(response);

            }, function(err){
              CoreService.toastError(
                'Error deleting tag',
                'Your tag is not deleted: ' + err);
              deferred.reject(err);
            });
          },
          function(){
            deferred.reject('Action cancelled');
            return false;
          });

    return deferred.promise;
  };

}]);
