'use strict';
angular.module('com.module.tags')
.controller('TagsCtrl', function($scope, Tag, TagsService){

  $scope.loading = true;

  function loadTags(){
      $scope.tags = Tag.find(function(){
        $scope.loading = false;
      });
    }

  $scope.delete = function(id){
    return TagsService.deleteTag(id)
        .then(function(response){
          console.log(response);
          loadTags();
        });
  };

  loadTags();

});
