'use strict';
angular.module('com.module.tags')
  .run(function($rootScope, Tag){

    Tag.count(function(data){
      $rootScope.addDashboardBox('Tags',
        'bg-red', 'ion-pricetag', data.count, 'app.tags.list');
    });

  });
