'use strict';
angular.module('com.module.tags')
  .config(function($stateProvider){
    $stateProvider.state('app.tags', {
        abstract: true,
        url: '/tags',
        templateUrl: 'modules/tags/views/main.html'
      })
      .state('app.tags.list', {
        url: '',
        templateUrl: 'modules/tags/views/list.html',
        controller: 'TagsCtrl'
      });
  });
