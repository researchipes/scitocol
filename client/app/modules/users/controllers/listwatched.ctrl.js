'use strict';
angular.module('com.module.users')
  .controller('WatchedCtrl', function($scope, $stateParams, WatchersService){

    WatchersService.listProtocols($stateParams.id)
    .then(function(protos){
      $scope.protocols = protos;
    });

  });
