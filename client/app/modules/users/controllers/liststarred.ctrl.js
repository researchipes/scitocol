'use strict';
angular.module('com.module.users')
  .controller('StarredCtrl', function($scope, $stateParams, StarsService){

    StarsService.listProtocols($stateParams.id)
    .then(function(protos){
      $scope.protocols = protos;
    });

  });
