'use strict';
angular.module('com.module.users')
  .controller('ProfileCtrl', function($scope, $timeout, CoreService, User,
      gettextCatalog, Upload){

    $scope.user = User.getCurrent(function(user){
      $scope.data = {
        file: '',
        progress: '',
        done: '' ,
        picurl: 'https://s3-eu-west-1.amazonaws.com/scitocols-profile-photos/' +
          $scope.user.id + '.jpg'
      };
    }, function(err){
      console.log(err);
    });

    $scope.formFields = [{
      key: 'username',
      type: 'text',
      label: gettextCatalog.getString('Username'),
      required: true
    }, {
      key: 'email',
      type: 'email',
      label: gettextCatalog.getString('E-mail'),
      required: true
    }, {
      key: 'firstName',
      type: 'text',
      label: gettextCatalog.getString('First name'),
      required: true
    }, {
      key: 'lastName',
      type: 'text',
      label: gettextCatalog.getString('Last name'),
      required: true
    }];

    $scope.formOptions = {
      uniqueFormId: true,
      hideSubmit: false,
      submitCopy: gettextCatalog.getString('Save')
    };

    $scope.onSubmit = function(){
      User.upsert($scope.user, function(){
        CoreService.toastSuccess(gettextCatalog.getString(
          'Profile saved'), gettextCatalog.getString(
          'Enjoy the new you!'));
      }, function(err){
        CoreService.toastError(gettextCatalog.getString(
          'Error saving profile'), gettextCatalog.getString(
          'Your profile is not saved: ') + err);
      });
    };

    $scope.myImage = '';
    $scope.myCroppedImage = '';

    var handleFileSelect = function(evt){
      var file = evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function(evt){
        $scope.$apply(function($scope){
          $scope.myImage = evt.target.result;
        });
      };
      reader.readAsDataURL(file);
    };
    angular.element(document.querySelector('#fileInput')).on('change',
      handleFileSelect);

    var S3_BUCKET_URL = CoreService.env.apiUrl + '/Containers/' +
      'scitocols-profile-photos' + '/upload';

    $scope.uploadPicture = function(){

      var file = dataURItoBlob($scope.myCroppedImage);

      Upload.upload({
        url: S3_BUCKET_URL,
        file: file,
        fileName: $scope.user.id + '.jpg'
      }).progress(function(evt){
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        $scope.data.progress = progressPercentage;
      }).success(function(data, status, headers, config){
        $timeout(function(){
          $('#uploadModal').modal('hide');
          $scope.data.progress = 0;
          $scope.data.picurl = $scope.myCroppedImage;
          $scope.myCroppedImage = '';
          $scope.myImage = '';
          CoreService.setCacheBuster();
          $('#fileInput').wrap('<form>').closest('form').get(0).reset();
          $('#fileInput').unwrap();
          $scope.myprofilephoto.url = 'https://s3-eu-west-1.amazonaws.com/' +
            'scitocols-profile-photos/' + $scope.user.id + '.jpg?' +
            CoreService.getCacheBuster();

        }, 3000);

      }).error(function(data, status, headers, config){
        console.log('error status: ' + status);
      });

    };

    $scope.cancelUpload = function(){
      $scope.myCroppedImage = '';
      $scope.myImage = '';

      $('#uploadModal').modal('hide');

    };

    var dataURItoBlob = function(dataURI){
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for (var i = 0; i < binary.length; i++) {
          array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: mimeString});
      };

  });
