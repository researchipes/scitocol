'use strict';
/**
 * @ngdoc function
 * @name com.module.users.controller:RegisterCtrl
 * @description Login Controller
 * @requires $scope
 * @requires $routeParams
 * @requires $location
 * Controller for Register Page
 **/
angular.module('com.module.users')
  .controller('ResetCtrl', function($scope, $routeParams, $location, $filter,
    CoreService, User, AppAuth, gettextCatalog){

    $scope.registration = {
      password: ''
    };

    $scope.schema = [
      {
        label: '',
        property: 'password',
        placehodler: gettextCatalog.getString('New Password'),
        type: 'password',
        attr: {
          required: true,
          ngMinlength: 6
        }
      }
    ];

    $scope.options = {
      validation: {
        enabled: true,
        showMessages: false
      },
      layout: {
        type: 'basic',
        labelSize: 3,
        inputSize: 9
      }
    };

    $scope.reset = function(){

      User.saveNewPassword(
        {access_token: $location.search().access_token, // jscs:ignore requireCamelCaseOrUpperCaseIdentifiers
         newpassword: $scope.registration.password}, function(updUser){
        if (!updUser.changeduser){
          CoreService.toastError(gettextCatalog.getString(
            'Error resetting password'), gettextCatalog.getString(
            'Please try again! '));
        }
        else {
          $location.nextAfterLogin = '/app';
          CoreService.toastSuccess(gettextCatalog.getString(
            'Password successfully resetted'), gettextCatalog.getString(
            'You are good to go now!'));
          $location.path('/login');
        }
      });

    };

  });
