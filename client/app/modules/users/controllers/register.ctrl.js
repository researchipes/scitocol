'use strict';
/**
 * @ngdoc function
 * @name com.module.users.controller:RegisterCtrl
 * @description Login Controller
 * @requires $scope
 * @requires $routeParams
 * @requires $location
 * Controller for Register Page
 **/
angular.module('com.module.users')
  .controller('RegisterCtrl', function($scope, $routeParams, $location, $filter,
    CoreService, User, AppAuth, gettextCatalog){

    $scope.registration = {
      firstName: '',
      lastName: '',
      email: '',
      password: ''
    };

    $scope.schema = [
      {
        type: 'multiple',
        fields: [{
          label: '',
          property: 'firstName',
          placeholder: gettextCatalog.getString('First Name'),
          type: 'text',
          attr: {
            required: false
          },
        }, {
          label: '',
          property: 'lastName',
          placeholder: gettextCatalog.getString('Last Name'),
          type: 'text',
          attr: {
            required: false
          },
        }],
        columns: 6

      },
      {
        label: '',
        property: 'username',
        placeholder: gettextCatalog.getString('Username*'),
        type: 'text',
        attr: {
          required: true,
          ngMinlength: 3
        },
        msgs: {
          required: gettextCatalog.getString(
            'This helps you to interact faster!')
        }
      },
      {
        label: '',
        property: 'email',
        placeholder: gettextCatalog.getString('Email*'),
        type: 'email',
        attr: {
          required: true,
          ngMinlength: 4
        },
        msgs: {
          required: gettextCatalog.getString(
            'Don\'t worry we won\'t spam your inbox!'),
          email: gettextCatalog.getString('Email address needs to be valid'),
          valid: gettextCatalog.getString('Nice email address!')
        }
      },

      {
        label: '',
        property: 'password',
        placehodler: gettextCatalog.getString('Password*'),
        type: 'password',
        attr: {
          required: true,
          ngMinlength: 6
        },
        msgs: {
          minlength: gettextCatalog.getString(
            'Needs to have at least 6 characters!')
        }
      }
    ];

    $scope.options = {
      validation: {
        enabled: true,
        showMessages: false
      },
      layout: {
        type: 'basic',
        labelSize: 3,
        inputSize: 9
      }
    };

    $scope.register = function(){

      $scope.user = User.save($scope.registration,
        function(){

          $scope.loginResult = User.login({
              include: 'user',
              rememberMe: true
            }, $scope.registration,
            function(){
              AppAuth.currentUser = $scope.loginResult.user;
              CoreService.toastSuccess(gettextCatalog.getString(
                'Registered'), gettextCatalog.getString(
                'You are registered!'));
              $location.path('/');
            },
            function(res){
              CoreService.toastWarning(gettextCatalog.getString(
                  'Error signin in after registration!'), res.data.error
                .message);
              $scope.loginError = res.data.error;
            }
          );

        },
        function(res){
          CoreService.toastError(gettextCatalog.getString(
            'Error registering!'), res.data.error.message);
          $scope.registerError = res.data.error;
        }
      );
    };

  });
