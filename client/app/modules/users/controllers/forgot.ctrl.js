'use strict';
/**
 * @ngdoc function
 * @name com.module.users.controller:LoginCtrl
 * @description Login Controller
 * @requires $scope
 * @requires $routeParams
 * @requires $location
 * Contrller for Login Page
 **/
angular.module('com.module.users')
  .controller('ForgotCtrl', function($scope, $routeParams, $location, $filter,
    CoreService, User, AppAuth, gettextCatalog){

    $scope.credentials = {
      email: ''
    };

    $scope.schema = [{
      label: '',
      property: 'email',
      placeholder: gettextCatalog.getString('Email'),
      type: 'email',
      attr: {
        required: true,
        ngMinlength: 4
      },
      msgs: {
        required: gettextCatalog.getString('You need an email address'),
        email: gettextCatalog.getString('Email address needs to be valid'),
        valid: gettextCatalog.getString('Nice email address!')
      }
    }];

    $scope.options = {
      validation: {
        enabled: true,
        showMessages: false
      },
      layout: {
        type: 'basic',
        labelSize: 3,
        inputSize: 9
      }
    };

    $scope.sendResetPass = function(){

      User.resetPassword({
        email: $scope.credentials.email
      }, function(err){
        if (err){
          console.log(err);
        }

        CoreService.toastSuccess(gettextCatalog.getString(
          'Sent password reset link'), gettextCatalog.getString(
          'Please follow instructions in email!'));

        var next = $location.nextAfterLogin || '/';
        $location.nextAfterLogin = null;
        if (next === '/welcome') {
          next = '/';
        }
        $location.path(next);
      });

    };

  });
