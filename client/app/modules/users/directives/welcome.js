'use strict';

/**
 * @ngdoc directive
 * @name com.module.core.directive:welcome
 * @description
 * # register
 */
angular.module('com.module.users')
  .directive('welcome', function(){
    return {
      templateUrl: 'modules/users/views/welcome.html',
      restrict: 'E'
    };
  });
