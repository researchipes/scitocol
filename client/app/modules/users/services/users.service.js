'use strict';
var app = angular.module('com.module.users');

app.service('UserService',['User', function(User){

  this.getTeams = function(){
    return User.getTeams().$promise
    .then(function(data){
      return data.teams;
    })
    .catch(function(error){
      console.log('error fetching teams', error);
    });
  };

  this.getUserInfo = function(userId){
    return User.findById({
      id: userId,
      filter: {fields: {id: true, username: true, firstName: true,
        lastName: true, verificationToken: false}}}, function(data){

        }, function(err){
      console.log(err);
    }).$promise;
  };

}]);
