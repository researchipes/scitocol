'use strict';
/**
 * @ngdoc directive
 * @name com.module.core.directive:adminBox
 * @description
 * # adminBox
 */
angular.module('com.module.protocols')
  .directive('protocolListgrid', function(){
    return {
      templateUrl: 'modules/protocols/directives/protocolListGridTemplate.html'
    };
  });
