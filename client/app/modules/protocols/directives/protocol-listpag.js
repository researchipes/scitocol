'use strict';
/**
 * @ngdoc directive
 * @name com.module.core.directive:adminBox
 * @description
 * # adminBox
 */
angular.module('com.module.protocols')
  .directive('protocolListpag', function(){
    return {
      templateUrl: 'modules/protocols/directives/protocolListPagTemplate.html'
    };
  });
