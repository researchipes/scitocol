'use strict';
/**
 * @ngdoc directive
 * @name com.module.protocols.directive:protocolList
 * @description
 * # protocolList
 */
angular.module('com.module.protocols')
  .directive('protocolList', function(){
    return {
      templateUrl: 'modules/protocols/directives/protocolListTemplate.html'
    };
  });
