'use strict';
/**
 * @ngdoc directive
 * @name com.module.core.directive:adminBox
 * @description
 * # adminBox
 */
angular.module('com.module.protocols')
  .directive('protocolListsmallx', function(){
    return {
      templateUrl: 'modules/protocols/directives/protocolListSmallTemplate2.html', // jscs:ignore maximumLineLength
      scope: {
        protocols: '=',
        boxtitle: '@'
      }
    };
  });
