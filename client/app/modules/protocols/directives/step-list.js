'use strict';
/**
 * @ngdoc directive
 * @name com.module.protocols.directive:stepList
 * @description
 * # stepList
 */
angular.module('com.module.protocols')
  .directive('stepList', function(){
    return {
      templateUrl: 'modules/protocols/directives/stepListTemplate.html'
    };
  });
