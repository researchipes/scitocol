'use strict';
/**
 * @ngdoc directive
 * @name com.module.core.directive:adminBox
 * @description
 * # adminBox
 */
angular.module('com.module.protocols')
  .directive('protocolListsmall', function(){
    return {
      templateUrl: 'modules/protocols/directives/protocolListSmallTemplate.html'
    };
  });
