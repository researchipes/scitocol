'use strict';
angular.module('com.module.protocols')
  .config(function($stateProvider){
    $stateProvider.state('app.protocols', {
        abstract: true,
        url: '/protocols',
        templateUrl: 'modules/protocols/views/main.html'
      })
      .state('app.protocols.list', {
        url: '',
        templateUrl: 'modules/protocols/views/list.html',
        controller: 'ProtocolListCtrl',
        params: {tags: [], materials: [], equipment: [], listPublic: false}
      })
      .state('app.protocols.add', {
        url: '/add',
        params: {previewed: false, ownerType: 'user', ownerId: ''},
        templateUrl: 'modules/protocols/views/form.html',
        controller: 'ProtocolFormCtrl'
      })
      .state('app.protocols.edit', {
        url: '/:id/edit',
        params: {previewed: false},
        templateUrl: 'modules/protocols/views/form.html',
        controller: 'ProtocolFormCtrl'
      })
      .state('app.protocols.view', {
        url: '/:id&:version',
        params: {version: ''},
        templateUrl: 'modules/protocols/views/view.html',
        controller: 'ProtocolViewCtrl'
      })
      .state('app.protocols.starring', {
        url: '/starring/:protoId',
        params: {protoId: ''},
        templateUrl: 'modules/protocols/views/starring.html',
        controller: 'ProtocolStarringCtrl'
      })
      .state('app.protocols.watching', {
        url: '/watching/:protoId',
        params: {protoId: ''},
        templateUrl: 'modules/protocols/views/watching.html',
        controller: 'ProtocolWatchingCtrl'
      })
      .state('app.protocols.preview', {
        url: '/preview',
        templateUrl: 'modules/protocols/views/preview.html',
        controller: 'ProtocolPreviewCtrl'
      });
  });
