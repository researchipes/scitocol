'use strict';
angular.module('com.module.protocols')
  .run(function($rootScope, Protocol){

    Protocol.countInMine(function(data){
      $rootScope.addDashboardBox('My protocols',
        'bg-purple', 'ion-erlenmeyer-flask', data.count, 'app.protocols.list');
    });

    Protocol.countPublic(function(data){
      $rootScope.addDashboardBox('Public protocols',
        'bg-purple', 'ion-erlenmeyer-flask',
        data.count, 'app.protocols.list({listPublic:true})');
    });

  });
