'use strict';
var app = angular.module('com.module.protocols');

app.service('ProtocolsService',['Protocol', 'User', 'CoreService',
'LoopBackAuth', '$state', '$q', function(Protocol, User, CoreService,
LoopBackAuth, $state, $q){

  var stashedProtocol = {};

  this.stashProtocol = function(protocol){
    //Will this work?
    this.stashedProtocol = _.extend({},protocol);
  }

  this.popProtocol = function(){
    return this.stashedProtocol;
  }

  this.deleteProtocol = function(id){
    CoreService.confirm('Are you sure?',
    'Deleting this cannot be undone',
      function(){
        Protocol.deleteById(id, function(){
    $state.go('app.protocols.list');
    CoreService.toastSuccess(
    'Protocol deleted',
            'Your protocol is deleted!');

  }, function(err){
    CoreService.toastError(
    'Error deleting protocol',
            'Your protocol is not deleted: ' + err);
  });
      },
      function(){
        return false;
      });
  };

  this.getProtocol = function(protocolId){

    return Protocol.findById({
      id: protocolId,
      filter: {include: ['steps', {'commits': 'user'}, 'tags', 'materials',
                         'equipment', 'stars', 'watchers']}
    }, function(data){},
    function(err){
      console.log(err);
    }).$promise;

  };

  this.getProtocolForEditing = function(protocolId){

    return Protocol.findById({
      id: protocolId,
      filter: {include: ['steps', 'tags', 'materials', 'equipment',
                         'stars','watchers']}
    }, function(data){},
    function(err){
      console.log(err);
    }).$promise;

  };

  // Search for all protocols with filter.
  // We can't use this function with pagination!
  this.find = function(filter){

    var privateFilter = _.deepExtend(
      {filter: {where: {isPublic: false}}},
      filter);

    var publicProtocolsPromise = Protocol.findPublic(filter).$promise;
    var privateProtocolsPromise = Protocol.findInMine(privateFilter).$promise;

    return $q.all([publicProtocolsPromise,privateProtocolsPromise])
    .then(function(response){
      return response[0].protocols.concat(response[1].protocols);
    });

  };

  // Search for a protocol with searchterm val, partially matching field
  this.searchAll = function(val, field){

    //This is some JSON magic...
    var filt = {};
    // The 'i' stands for case insensitive
    filt[field] = {'like': String(val), 'options': 'i'};
    var params = {'filter': {'where': filt}};
    return this.find(params);

  };

  // Search public protocols. This just wraps Protocol.findPublic,
  // but it returns list of protocols instead of {protocols: [...]}
  this.findPublic = function(filter){
    return Protocol.findPublic(filter).$promise
    .then(function(response){
      return response.protocols;
    });
  };

  // Search in user's protocols. This just wraps Protocol.findInMine,
  // but it returns list of protocols instead of {protocols: [...]}
  this.findInMine = function(filter){
    return Protocol.findInMine(filter).$promise
    .then(function(response){
      return response.protocols;
    });
  };

  // And a helper function for searching public protocols
  this.searchPublic = function(val, field){
    //This is some JSON magic...
    var filt = {};
    //The 'i' stands for case insensitive
    filt[field] = {'like': String(val), 'options': 'i'};
    var params = {'filter': {'where': filt}};
    return this.findPublic(params);

  };

  // And a helper function for searching public protocols
  this.searchInMine = function(val, field){
    //This is some JSON magic...
    var filt = {};
    //The 'i' stands for case insensitive
    filt[field] = {'like': String(val), 'options': 'i'};
    var params = {'filter': {'where': filt}};
    return this.findInMine(params);

  };



}]);
