'use strict';
angular.module('com.module.protocols')
  .controller('ProtocolListCtrl', function($scope, $stateParams, CoreService,
  Protocol, Tag, ProtocolsService){

    angular.module('infinite-scroll').value('THROTTLE_MILLISECONDS', 250);

    $scope.loading = true;

    $scope.scrolllimit = 12;
    $scope.scrollstate = 12;
    $scope.busyLoadingData = false;

    $scope.showingcount = 12;
    $scope.offsetcount = 0;
    $scope.totalcount = 1;
    $scope.pageSize = 12;

    $scope.pagination = {
      current: 1
    };

    var findFunction;
    var countFunction;
    if ($stateParams.listPublic) {
      findFunction = function(argIn){
        return ProtocolsService.findPublic(argIn);
      };

      countFunction = function(){
        return Protocol.countPublic();
      };
    }
    else {
      findFunction = function(argIn){
        return ProtocolsService.findInMine(argIn);
      };

      countFunction = function(){
        return Protocol.countInMine();
      };
    }

    $scope.cacheBuster = function(){
      return CoreService.getCacheBuster();
    };

    if ($stateParams.tags.length > 0) {
      var tagIds = $stateParams.tags.map(function(obj){return obj.id;});
      Protocol.searchPublicByTags({tagIds: tagIds}).$promise
      .then(
        function(response){
          $scope.protocols = response.protocols;
          $scope.totalcount = response.protocols.length;
          $scope.loading = false;
        },
        function(error){
          console.log(error);
        }

      );
    }
    else if ($stateParams.materials.length > 0) {
      var materialIds = $stateParams.materials.map(function(obj){return obj.id;});
      Protocol.searchPublicByMaterials({materialIds: materialIds}).$promise
      .then(
        function(response){
          $scope.protocols = response.protocols;
          $scope.totalcount = response.protocols.length;
          $scope.loading = false;
        },
        function(error){
          console.log(error);
        }

      );
    }
    else if ($stateParams.equipment.length > 0) {
      var equipmentIds = $stateParams.equipment.map(function(obj){return obj.id;});
      Protocol.searchPublicByEquipment({equipmentIds : equipmentIds }).$promise
      .then(
        function(response){
          $scope.protocols = response.protocols;
          $scope.totalcount = response.protocols.length;
          $scope.loading = false;
        },
        function(error){
          console.log(error);
        }

      );
    }
    else {
      $scope.busyLoadingData = true;
      countFunction().$promise
      .then(function(amount){
          $scope.totalcount = amount.count;
          // This will initialize the protocol list when we load the list
          // for the first time
          $scope.goToPage(1);
        },
        function(error){
          console.log(error);
        }
      );
    }

    $scope.loadMore = function(){
      if ($scope.busyLoadingData) {
        return;
      }
      $scope.busyLoadingData = true;
      findFunction(
        {filter: {limit: $scope.scrolllimit,
                  offset: $scope.scrollstate}})
      .then(function(response){
          $scope.protocols = $scope.protocols.concat(response);
          $scope.scrollstate = $scope.scrollstate + $scope.scrolllimit;
          $scope.busyLoadingData = false;
          $scope.loading = false;
        },
        function(error){
          console.log(error);
        }
      );
    };

    $scope.pagPrev = function(){
      changePage($scope.showingcount, $scope.scrollstate - $scope.showingcount);
    };

    $scope.pagNext = function(){
      changePage($scope.showingcount, $scope.scrollstate + $scope.showingcount);
    };

    $scope.goToPage = function(pageNr){
      changePage($scope.showingcount, $scope.showingcount * (pageNr - 1));
    };

    function changePage(newLimit, newOffset){
      findFunction(
        {filter: {order: 'created DESC', limit: newLimit, offset: newOffset,
         include: 'owner'}})
      .then(function(response){
          $scope.protocols = response;
          $scope.loading = false;
          $scope.busyLoadingData = false;
        },
        function(error){
          console.log(error);
        }
      );
    }

  });
