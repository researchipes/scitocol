'use strict';
angular.module('com.module.protocols')
  .controller('ProtocolWatchingCtrl', function($scope, $stateParams, WatchersService){

    WatchersService.listUsers($stateParams.protoId)
    .then(function(users){
      $scope.users = users;
      $scope.protoId = $stateParams.protoId;
    });

  });