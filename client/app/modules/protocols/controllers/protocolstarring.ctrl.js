'use strict';
angular.module('com.module.protocols')
  .controller('ProtocolStarringCtrl', function($scope, $stateParams, StarsService){

    StarsService.listUsers($stateParams.protoId)
    .then(function(users){
      $scope.users = users;
      $scope.protoId = $stateParams.protoId;
    });

  });