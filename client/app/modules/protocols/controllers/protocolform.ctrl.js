'use strict';
angular.module('com.module.protocols')
.controller('ProtocolFormCtrl', function($scope, $state, $stateParams, $timeout,
$filter, $q, CoreService, ProtocolsService, Protocol, Step, User,
UserService, LoopBackAuth, Upload, SearchService, $window){

  var isNewProtocol = false;
  var protocolId = $stateParams.id;
  $scope.loading = true;

  // Set up so that we can monitor the scroll state, allowing the menus to
  // scroll for a bit, then float in a fixed position.
  $scope.scroll = 100;
  var windowEl = angular.element($window);
  var handler = function() {
    $scope.scroll = 60 - $window.Math.min(windowEl.scrollTop(), 50);
  }
  windowEl.on('scroll', $scope.$apply.bind($scope, handler));
  handler();

  // Set up $scope.protocol depending on $state

  if($stateParams.previewed){
    $scope.protocol = ProtocolsService.popProtocol();
    $scope.loading = false;
  }
  else if ($state.current.name === 'app.protocols.edit') {
    var protocolPromise = ProtocolsService.getProtocolForEditing(protocolId)
    .then(function(response){
      $scope.protocol = response.toJSON();
      $scope.protocol.coverphoto =
        'https://s3-eu-west-1.amazonaws.com/scitocols-protocol-coverphotos/' +
        protocolId + '.jpg?' + CoreService.getCacheBuster();

      if(!($scope.protocol.hasOwnProperty("references"))){
        $scope.protocol.references = [""];
      } 
    });

  }
  else {
    isNewProtocol = true;
    $scope.protocol = {
      'title': '',
      'description': '',
      'coverphoto': false,
      'isPublic': false,
      'references': [""],
      'result': ''
    };

    $scope.protocol.steps = [{'text': ''}];

    // Materials, equipments and tags
    $scope.protocol.materials = [];
    $scope.protocol.equipment = [];
    $scope.protocol.tags = [];

    if ($stateParams.ownerType === 'user') {
      $stateParams.ownerId = LoopBackAuth.currentUserId;
    }

    $scope.protocol.ownerType = $stateParams.ownerType;
    $scope.protocol.ownerId = $stateParams.ownerId;

  }

  $scope.goToPreview = function(){
    ProtocolsService.stashProtocol($scope.protocol);
    $state.go('app.protocols.preview');
  }

  $scope.getTags = function(val){
      return SearchService.searchTag(val, 'text');
    };

  $scope.getMaterials = function(val){
      return SearchService.searchMaterials(val, 'name');
    };

  $scope.getEquipment = function(val){
      return SearchService.searchEquipment(val, 'name');
    };

  $scope.asyncProtocol = '';

  $scope.searchProtocols = function(val){
    return ProtocolsService.searchAll(val, 'title');
  };

  $scope.asyncCrossRef = "";
  $scope.searchCrossRef = function(val){
    return SearchService.searchCrossRef(val);
  }
  $scope.selectCrossRef = function(){
    console.log($scope.asyncCrossRef)
    $scope.protocol.references[0] = $scope.asyncCrossRef.doi;
  }

  $scope.selectNestedProtocol = function(protocolId, protocolTitle){
    $scope.asyncProtocol = '';
    $scope.nestedProtocolId = protocolId;
    $scope.nestedProtocolTitle = protocolTitle;
  };

  $scope.cancelNestedProtocol = function(){
    $scope.asyncProtocol = '';
    $scope.nestedProtocolId = '';
    $scope.nestedProtocolTitle = '';
  };

  $scope.addNestedProtocol = function(){
    ProtocolsService.getProtocolForEditing($scope.nestedProtocolId)
      .then(function(response){
        var nestedProtocol = response.toJSON();
        nestedProtocol.steps.forEach(function(entry){
          $scope.protocol.steps.push({title: entry.title,text: entry.title,
            bucket: entry.bucket});

        });
        nestedProtocol.tags.forEach(function(entry){

          if ($scope.protocol.tags.map(function(x){return x.text; }).
            indexOf(entry.text) === -1){
            $scope.protocol.tags.push(entry);

          }
        });
        nestedProtocol.materials.forEach(function(entry){
          if ($scope.protocol.materials.map(function(x){return x.name; }).
            indexOf(entry.name) === -1){
            $scope.protocol.materials.push(entry);
          }
        });
        nestedProtocol.equipment.forEach(function(entry){
          if ($scope.protocol.equipment.map(function(x){return x.name; }).
            indexOf(entry.name) === -1){
            $scope.protocol.equipment.push(entry);
          }
        });

        var exectime = parseInt($scope.protocol.executiontime) || 0;
        var nestedTime = parseInt(nestedProtocol.executiontime) || 0;

        $scope.protocol.executiontime = exectime + nestedTime;

        $scope.cancelNestedProtocol();

      })
      .catch(function(error){
        console.log(error);
      });

  };

  $scope.linkNestedProtocol = function(){
    $scope.protocol.steps.push({title: 'Inlcuded protocol: ' +
      $scope.nestedProtocolTitle , text: 'Follow all the steps in protocol: [' +
      $scope.nestedProtocolTitle + '](/#/app/protocols/' +
      $scope.nestedProtocolId + '&)'});
    $scope.cancelNestedProtocol();
  };

  // Start creating list of possible owners of a protocol (the user and his
  // teams.
  $scope.ownerChoices = [{ownerType: 'user',
    ownerId: LoopBackAuth.currentUserId, ownerName: 'You'}];

  // This loads teams that user is member of, such that you can choose ownership
  // of protocol:
  var teamPromise = UserService.getTeams()
  .then(function(teams){
    var allTeams = teams.owned.concat(teams.joined);
    var team = {};
    for (team in allTeams) {
      $scope.ownerChoices = $scope.ownerChoices.concat({ownerType: 'team',
        ownerId: allTeams[team].id, ownerName: allTeams[team].name});
    }
  });

  // Could this actually cause a problem if protocolPromise isn't defined? I.e.
  // if we are creating a new protocol?
  $q.all([protocolPromise, teamPromise])
  .then(function(response){

    if ($state.current.name === 'app.protocols.edit') {
      $scope.ownerChoiceIdx = ($scope.ownerChoices.map(function(obj){
        return String(obj.ownerId);
      })).indexOf($scope.protocol.ownerId);
    }
    else {
      $scope.ownerChoiceIdx = ($scope.ownerChoices.map(function(obj){
        return String(obj.ownerId);
      })).indexOf(String($stateParams.ownerId));
      $scope.loading = false;
    }

    $scope.loading = false;

  });

  $scope.ownerChoiceFn = function(idx){
    $scope.ownerChoiceIdx = idx;
    $scope.protocol.ownerType = $scope.ownerChoices[idx].ownerType;
    $scope.protocol.ownerId = $scope.ownerChoices[idx].ownerId;
  };

  $scope.delete = function(id){

    ProtocolsService.deleteProtocol(id);

  };

  $scope.onSubmit = function(){
    // Prepare protocol for upserting
    var cleanName = $scope.protocol.title.replace(/[^a-zA-Z0-9\-\s]/g, '');

    // Add order number to each step, as array order is not conserved when
    // uploading. Here, parseInt is necessary, as for some reason the 0 index
    // becomes '0'.
    for (var step in $scope.protocol.steps) {
      $scope.protocol.steps[step].order = parseInt(step);
    }

    $scope.protocol.protocolSteps = $scope.protocol.steps;
    $scope.protocol.protocolTags = $scope.protocol.tags;
    $scope.protocol.protocolMaterials = $scope.protocol.materials;
    $scope.protocol.protocolEquipment = $scope.protocol.equipment;

    if (isNewProtocol) {
      upsertProtocol('Initial version');
    }
    else {
      CoreService.commit('Commit changes',
        'Please enter a short description of the changes that you made',
        function(inputValue){
          upsertProtocol(inputValue);
        },
        function(){
          return false;
        });
    }
  };

  var upsertProtocol = function(msg){
    // TODO: This is hack for now, should really be added into context,
    // not part of protocol
    $scope.protocol.commitMsg = msg;
    Protocol.upsert($scope.protocol).$promise
    .then(function(response){
      CoreService.toastSuccess('Protocol saved!');
      $state.go('^.view',{id: response.id});
    },
      function(error){
        console.log(error);
      });
  };

  $scope.changeOwner = function(newOwner){
    Protocol.upsert(
      {id: $stateParams.id,
      ownerType: newOwner.ownerType,
      ownerId: newOwner.ownerId,
      commitMsg: 'Changing owner of the protocol.'},
      function(){
        CoreService.toastSuccess('Protocol owner changed',
          'Your protocol is safe with us!');
        $state.go('^.view',{id: $stateParams.id});
      }, function(err){
        console.log(err);
      });
  };

  $scope.addStep = function(){
    $scope.protocol.steps.push({'text': ''});
  };

  $scope.removeStep = function(index){
    CoreService.confirm('Confirm step removal',
      'Are you sure you want to remove step ' + String(index + 1) + '?',
      function(yes){
        $scope.protocol.steps.splice(index,1);
      },
      function(no){});

  };

  $scope.dragControlListeners = {
    orderChanged: function(event){console.log($scope.steps);}
  };

  //cover photo upload stuff
  $scope.myImage = '';
  $scope.myCroppedImage = '';

  $scope.data = {
      file: '',
      progress: '',
      done: ''
    };

  var handleFileSelect = function(evt){
    var file = evt.currentTarget.files[0];
    var reader = new FileReader();
    reader.onload = function(evt){
      $scope.$apply(function($scope){
        $scope.myImage = evt.target.result;
      });
    };
    reader.readAsDataURL(file);
  };
  angular.element(document.querySelector('#fileInput')).on('change',
    handleFileSelect);

  var S3_BUCKET_URL = CoreService.env.apiUrl + '/Containers/' +
    'scitocols-protocol-coverphotos' + '/upload';

  $scope.uploadPicture = function(){

    var file = dataURItoBlob($scope.cropper.croppedImage);

    Upload.upload({
      url: S3_BUCKET_URL,
      file: file,
      fileName: $stateParams.id + '.jpg'
    }).progress(function(evt){
      var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
      $scope.data.progress = progressPercentage;
    }).success(function(data, status, headers, config){
      $timeout(function(){
        $('#uploadModal').modal('hide');
        $scope.data.progress = 0;
        $scope.data.picurl = $scope.cropper.croppedImage;
        $scope.cropper.croppedImage = '';
        $scope.myImage = '';
        CoreService.setCacheBuster();
        $('#fileInput').wrap('<form>').closest('form').get(0).reset();
        $('#fileInput').unwrap();
        $scope.protocol.coverphoto =
          'https://s3-eu-west-1.amazonaws.com/scitocols-protocol-coverphotos/' +
           $stateParams.id + '.jpg?' + CoreService.getCacheBuster();

      }, 3000);

    }).error(function(data, status, headers, config){
      console.log('error status: ' + status);
    });

  };

  $scope.cancelUpload = function(){
    $scope.myCroppedImage = '';
    $scope.myImage = '';

    $('#uploadModal').modal('hide');

  };

  var dataURItoBlob = function(dataURI){
      var binary = atob(dataURI.split(',')[1]);
      var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
      var array = [];
      for (var i = 0; i < binary.length; i++) {
        array.push(binary.charCodeAt(i));
      }
      return new Blob([new Uint8Array(array)], {type: mimeString});
    };

});
