'use strict';
angular.module('com.module.protocols')
  .controller('ProtocolPreviewCtrl', function($scope, $state, ProtocolsService){

    $scope.isEditor = true;
    $scope.protocol = ProtocolsService.popProtocol();
    $scope.isPreview = true;

    $scope.goToEdit = function(){
      if($scope.protocol.id){
        $state.go('app.protocols.edit', {id: $scope.protocol.id, previewed: true})
      }
      else{
        $state.go('app.protocols.edit', {previewed: true})
      }
    }

  });
