'use strict';
angular.module('com.module.protocols')
  .controller('ProtocolViewCtrl', function($scope, $q, $stateParams,
  CoreService, Protocol, ProtocolsService, UserService, TeamsService,
  LoopBackAuth){
    $scope.scopeVar = $stateParams.id;

    $scope.protocolId = $stateParams.id;
    $scope.version = $stateParams.version;
    $scope.isPreview = false;

    $scope.isEditor = false;
    if ($scope.version) {
      Protocol.getVersion({protocolId: $scope.protocolId, oid: $scope.version},
        function(data){
          $scope.protocol = data.protocol;
          $scope.protocol.coverphoto = 'https://s3-eu-west-1.amazonaws.com' +
            '/scitocols-protocol-coverphotos/' + $stateParams.id + '.jpg?' +
            CoreService.getCacheBuster();
        },
        function(err){
          console.log(err);
        }
      );
      // .then(function(data){
      //   $scope.protocol = data.protocol;
      //   // TODO: Should we show previous version of cover photo?
      //   $scope.protocol.coverphoto = 'https://s3-eu-west-1.amazonaws.com' +
      //     '/scitocols-protocol-coverphotos/' + $stateParams.id + '.jpg?' +
      //     CoreService.getCacheBuster();
      // });

    }
    else {
      var protocolPromise = ProtocolsService.getProtocol($scope.protocolId)
      .then(function(response){
        $scope.protocol = response;
        $scope.protocol.coverphoto = 'https://s3-eu-west-1.amazonaws.com' +
          '/scitocols-protocol-coverphotos/' + $stateParams.id + '.jpg?' +
          CoreService.getCacheBuster();
        if (String($scope.protocol.ownerType) == 'user'){
          UserService.getUserInfo($scope.protocol.ownerId)
          .then(function(userData){
            $scope.protocolOwnerInfo = userData;
          });

        }
        else {
          TeamsService.getTeam($scope.protocol.ownerId)
          .then(function(teamData){
            $scope.protocolTeamInfo = teamData;
            console.log($scope.protocolTeamInfo);
          });

        }

      });
    }

    var teamsPromise = UserService.getTeams();

    //Check whether user is allowed to edit the protocol
    $q.all([protocolPromise, teamsPromise]).then(function(response){

      var teams = response[1];
      var teamIds = teams.joined.concat(teams.owned);
      var teamIds = teamIds.map(function(obj){return obj.id;});

      if (String($scope.protocol.ownerType) == 'user' &&
        String($scope.protocol.ownerId) == String(LoopBackAuth.currentUserId)){
        $scope.isEditor = true;
      }
      else if (String($scope.protocol.ownerType) == 'team' &&
        teamIds.indexOf(String($scope.protocol.ownerId)) > -1){
        $scope.isEditor = true;
      }

    });

    $scope.delete = function(id){
      ProtocolsService.deleteProtocol(id);
    };

  });
