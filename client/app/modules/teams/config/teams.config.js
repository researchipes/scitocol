'use strict';
angular.module('com.module.teams')
  .run(function($rootScope, Team, gettextCatalog){

    Team.count(function(data){
      $rootScope.addDashboardBox(gettextCatalog.getString('Teams'),
        'bg-yellow', 'ion-person-stalker', data.count, 'app.teams.list');
    });

  });
