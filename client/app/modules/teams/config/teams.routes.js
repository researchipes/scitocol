'use strict';
angular.module('com.module.teams')
  .config(function($stateProvider){
    $stateProvider.state('app.teams', {
        abstract: true,
        url: '/teams',
        templateUrl: 'modules/teams/views/main.html'
      })
      .state('app.teams.list', {
        url: '',
        templateUrl: 'modules/teams/views/list.html',
        controller: 'TeamListCtrl'
      })
      .state('app.teams.add', {
        url: '/add',
        templateUrl: 'modules/teams/views/form.html',
        controller: 'TeamFormCtrl'
      })
      .state('app.teams.edit', {
        url: '/:id/edit',
        templateUrl: 'modules/teams/views/form.html',
        controller: 'TeamFormCtrl'
      })
      .state('app.teams.view', {
        url: '/:id',
        templateUrl: 'modules/teams/views/view.html',
        controller: 'TeamViewCtrl'
      });
  });
