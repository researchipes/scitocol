'use strict';
angular.module('com.module.teams')
.controller('TeamViewCtrl', function($scope, $stateParams,
      Team, LoopBackAuth, TeamsService){

  $scope.loading = true;

  $scope.teamId = $stateParams.id;

  TeamsService.getTeamAndProtocols($scope.teamId)
  .then(
    function(team){

      $scope.team = team;

      $scope.isTeamMember = (team.members.map(function(obj){
          return obj.id;
        }).indexOf(String(LoopBackAuth.currentUserId))) > -1;

      $scope.isTeamOwner = team.owner.id === LoopBackAuth.currentUserId;

      $scope.protocols = team.protocols;

      $scope.loading = false;

    },
    function(err){console.log(err);});

  $scope.delete = function(id){
      return TeamsService.deleteTeam(id);
    };

});
