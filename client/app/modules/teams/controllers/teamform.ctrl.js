'use strict';
angular.module('com.module.teams')
.controller('TeamFormCtrl', function($scope, $state, $stateParams, $filter,
    CoreService, gettextCatalog, User, Team, LoopBackAuth, $http, SearchService,
    TeamsService){

  $scope.loading = true;

  $scope.teamId = $stateParams.id;

  $scope.userSearchTerm = '';

  //If teamId, then we are in the team edit view, so load all of that data
  if ($state.current.name === 'app.teams.edit') {
    $scope.creatingNew = false;
    TeamsService.getTeam($stateParams.id)
    .then(function(response){
      $scope.team = response;
    });

    $scope.loading = false;

  }
  else {
    $scope.creatingNew = true;
    $scope.team = {
      'name': '',
      'members': []
    };
    $scope.loading = false;
  }

  //Stuff for user search and add
  $scope.asyncUser = '';
  $scope.addTeamMember = function(member){
    $scope.asyncUser = '';
    $scope.team.members.push(member);
  };

  $scope.getUser = function(val){
    return SearchService.searchUser(val,'email');
  };

  $scope.onSubmit = function(){
    var cleanName = $scope.team.name.replace(/[^a-zA-Z0-9\-\s]/g, '');
    $scope.team.memberIds = $scope.team.members.map(function(obj){
        return obj.id;
      });
    // Make sure current user becomes the owner of the new Team
    $scope.team.ownerId = LoopBackAuth.currentUserId;

    var teamSubmit = {name: $scope.team.name,
      memberIds: $scope.team.members.map(function(obj){
        return obj.id;
      })
    };

    if ($state.current.name === 'app.teams.add') {
      teamSubmit.ownerId = LoopBackAuth.currentUserId;
    }
    if ($state.current.name === 'app.teams.edit') {
      teamSubmit.id = $scope.team.id;
    }

    Team.upsert(teamSubmit, function(){
      CoreService.toastSuccess(gettextCatalog.getString('Team saved'),
        gettextCatalog.getString('Your new team has been created!'));
      $state.go('^.list');
    }, function(err){
      console.log(err);
    });
  };

  $scope.delete = function(id){
    return TeamsService.deleteTeam(id);
  };

  //These two functions are not used anymore, but could put them in TeamsService
  //Add a member to the team
  $scope.addMember =  function(){
    Team.members.link({id: $scope.teamId, fk: $scope.addMemberId},null,
      function(data){
        $scope.addMemberId = '';
        loadMembers();
      },
      function(err){
        console.log(err);
      });
  };

  //Remove a member from the team
  $scope.unlinkMember = function(id){

    Team.members.unlink({id: $scope.teamId, fk: id},null,
      function(data){
        loadMembers();
      },
      function(err){
        console.log(err);
      });
  };

});
