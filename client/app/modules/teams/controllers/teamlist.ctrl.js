'use strict';
angular.module('com.module.teams')
.controller('TeamListCtrl', function($scope, $state, $stateParams,
  CoreService, gettextCatalog, Team, TeamsService, UserService){

  $scope.loading = true;

  function loadTeamList(){
    UserService.getTeams()
    .then(function(teams){
      $scope.teams = teams.owned.concat(teams.joined);
      $scope.loading = false;
    });
  }

  loadTeamList();

  $scope.delete = function(id){
    return TeamsService.deleteTeam(id);
  };
});
