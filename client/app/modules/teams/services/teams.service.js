'use strict';
var app = angular.module('com.module.teams');

app.service('TeamsService',['Team', 'CoreService', 'LoopBackAuth', '$state',
function(Team, CoreService, LoopBackAuth, $state){

  this.getTeam = function(teamId){

    return Team.findById({
      id: teamId, filter: {include: ['members','owner']}
    }, function(data){

    }, function(err){
      console.log(err);
    }).$promise;

  };

  this.getTeamAndProtocols = function(teamId){

    return Team.findById({
      id: teamId, filter: {include: ['members','owner', 'protocols']}
    }, function(data){

    }, function(err){
      console.log(err);
    }).$promise;

  };

  this.deleteTeam = function(id){
    CoreService.confirm('Are you sure?',
      'Deleting this cannot be undone',
      function(){
        Team.deleteById(id, function(){
          CoreService.toastSuccess(
            'Team deleted',
            'Your team is deleted!');
          $state.go('app.teams.list');
        }, function(err){
          CoreService.toastError(
            'Error deleting team',
            'Your team is not deleted: ' + err);
        });
      },
      function(){
        return false;
      });
  };

}]);
