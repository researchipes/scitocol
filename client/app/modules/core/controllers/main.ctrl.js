'use strict';
/**
 * @ngdoc function
 * @name com.module.core.controller:MainCtrl
 * @description Login Controller
 * @requires $scope
 * @requires $state
 * @requires $location
 * @requires CoreService
 * @requires AppAuth
 * @requires User
 * @requires gettextCatalog
 **/
angular.module('com.module.core')

  .controller('MainCtrl', function($scope, $rootScope, $state, $location,
  CoreService, User, ProtocolsService){

    $scope.menuoptions = $rootScope.menu;

    $scope.asyncProtocol = '';
    $scope.goToProtocol = function(protocolId){
      $scope.asyncProtocol = '';
    return $state.go('app.protocols.view',{id: protocolId}); };

    $scope.searchProtocols = function(val){
      return ProtocolsService.searchAll(val, 'title');
    };

    $scope.searchPublicProtocols = function(val){
      return ProtocolsService.searchPublic(val, 'title');
    };

    CoreService.setCacheBuster();

    $scope.getCacheBuster = function(){
      return CoreService.getCacheBuster();
    };

    $scope.menuoptions = $rootScope.menu;

    $scope.myprofilephoto = {
      url: ''
    };

    $scope.logout = function(){
      User.logout(function(){
        $state.go('welcome');
        CoreService.toastSuccess('Logged out',
          'You are logged out!');
      });
    };

  });
