'use strict';
/**
 * @ngdoc function
 * @name com.module.core.controller:HomeCtrl
 * @description Dashboard
 * @requires $scope
 * @requires $rootScope
 **/
angular.module('com.module.core')
  .controller('HomeCtrl', function($scope, $rootScope, CoreService, User,
    UserService, ProtocolsService){

    $scope.count = {};

    ProtocolsService.findPublic({
      filter: {
        order: 'created DESC',
        limit: 3
      }
    })
    .then(function(protocols){
      $scope.publicprotocols = protocols;
    });

    ProtocolsService.findInMine({
      filter: {
        order: 'created DESC',
        limit: 3
      }
    })
    .then(function(protocols){
      $scope.myprotocols = protocols;
    });

    $scope.loadingTeams = true;
    UserService.getTeams()
    .then(function(teams){
      $scope.userTeams = teams.owned.concat(teams.joined);
      $scope.loadingTeams = false;
    });

    $scope.currentUser = User.getCurrent(function(user){
      $scope.myprofilephoto.url =
        'https://s3-eu-west-1.amazonaws.com/scitocols-profile-photos/' +
        user.id + '.jpg?' + CoreService.getCacheBuster();
    }, function(err){
      console.log(err);
    });

  });
