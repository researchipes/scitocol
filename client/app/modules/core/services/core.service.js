'use strict';
var app = angular.module('com.module.core');

app.service('CoreService', ['ENV', 'SweetAlert', 'toasty', function(ENV,
  SweetAlert, toasty){

  this.env = ENV;

  this.cachBuster = '';

  this.alert = function(title, text){
    SweetAlert.swal(title, text);
  };

  this.alertSuccess = function(title, text){
    SweetAlert.swal(title, text, 'success');
  };

  this.alertError = function(title, text){
    SweetAlert.swal(title, text, 'error');
  };

  this.alertWarning = function(title, text){
    SweetAlert.swal(title, text, 'warning');
  };

  this.alertInfo = function(title, text){
    SweetAlert.swal(title, text, 'info');
  };

  this.confirm = function(title, text, successCb, cancelCb){
    var config = {
      title: title,
      text: text,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55'
    };
    this._swal(config, successCb, cancelCb);
  };

  this.commit = function(title, text, successCb, cancelCb){
    var config = {
      title: title,
      text: text,
      type: 'input',
      showCancelButton: true,
      confirmButtonColor: '#1D6B55',
      inputPlaceholder: 'Description of changes',
      animation: 'slide-from-top',
      closeOnConfirm: true
    };
    SweetAlert.swal(config,
      function(inputValue){
        if (inputValue === false) {
          return false;
        }

        if (inputValue === '') {
          swal.showInputError('You need to write something!');
          return false;
        }
        successCb(inputValue);
      });
  };

  this._swal = function(config, successCb, cancelCb){
    SweetAlert.swal(config,
      function(confirmed){
        if (confirmed) {
          successCb();
        }
        else {
          cancelCb();
        }
      });
  };

  this.toastSuccess = function(title, text){
    toasty.pop.success({
      title: title,
      msg: text,
      sound: false
    });
  };

  this.toastError = function(title, text){
    toasty.pop.error({
      title: title,
      msg: text,
      sound: false
    });
  };

  this.toastWarning = function(title, text){
    toasty.pop.warning({
      title: title,
      msg: text,
      sound: false
    });
  };

  this.toastInfo = function(title, text){
    toasty.pop.info({
      title: title,
      msg: text,
      sound: false
    });
  };

  this.setCacheBuster = function(){
    this.cacheBuster = new Date().getTime();
  };

  this.getCacheBuster = function(){
    return this.cacheBuster;
  };

}]);
