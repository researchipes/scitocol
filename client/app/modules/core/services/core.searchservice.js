'use strict';
var app = angular.module('com.module.core');

app.service('SearchService', ['$http','User', 'Tag','Material','Equipment',
function($http, User, Tag, Material, Equipment){

  //Search for a user with searchterm val, partially matching field
  this.searchUser = function(val, field){

    //This is some JSON magic...
    var filt = {};
    filt[field] = {'like': String(val), 'options': 'i'};
    var params = {'filter': {'where': filt}};
    return User.find(params).$promise;

  };

  //Search for a tag with searchterm val, partially matching field
  this.searchTag = function(val, field){

    //This is some JSON magic...
    var filt = {};
    //The 'i' stands for case insensitive
    filt[field] = {'like': String(val), 'options': 'i'};
    var params = {'filter': {'where': filt}};
    return Tag.find(params).$promise;

  };

  //Search for a material with searchterm val, partially matching field
  this.searchMaterials = function(val, field){

    //This is some JSON magic...
    var filt = {};
    //The "i" stands for case insensitive
    filt[field] = {'like': String(val), 'options': 'i'};
    var params = {'filter': {'where': filt}};
    return Material.find(params).$promise;

  };

  //Search for equipment with searchterm val, partially matching field
  this.searchEquipment = function(val, field){

    //This is some JSON magic...
    var filt = {};
    //The "i" stands for case insensitive
    filt[field] = {'like': String(val), 'options': 'i'};
    var params = {'filter': {'where': filt}};
    return Equipment.find(params).$promise;

  };

  //Search for user with searchterm val, partially matching field
  this.searchCrossRef = function(val) {

      return $http.get('http://search.crossref.org/dois?q='+String(val)).then(function(response) {return response.data;});
    
  };

}]);
