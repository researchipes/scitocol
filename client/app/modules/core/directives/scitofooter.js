'use strict';

/**
 * @ngdoc directive
 * @name com.module.core.directive:navbar
 * @description
 * # navbar
 */
angular.module('com.module.core')
  .directive('scitofooter', function(){
    return {
      templateUrl: 'modules/core/views/elements/scitofooter.html',
      replace: true,
      restrict: 'E'
    };
  });
