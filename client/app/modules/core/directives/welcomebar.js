'use strict';

/**
 * @ngdoc directive
 * @name com.module.core.directive:navbar
 * @description
 * # navbar
 */
angular.module('com.module.core')
  .directive('welcomebar', function(){
    return {
      templateUrl: 'modules/core/views/elements/welcomebar.html',
      replace: true,
      restrict: 'E'
    };
  });
