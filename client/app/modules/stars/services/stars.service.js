'use strict';
var app = angular.module('com.module.stars');

app.service('StarsService',['Star', 'CoreService', function(Star, CoreService){

  this.countStars = function(protoId){
    return Star.count({where: {'protocolId': protoId}}).$promise
    .then(function(response){
      return response.count;
    }).catch(function(error){
      console.log(error);
    });
  };

  this.myStar = function(protoId, userId){
    return Star.count(
      {where: {'protocolId': protoId,'userId': userId}}).$promise
    .then(function(response){
      return response.count > 0;
    })
    .catch(function(error){
      console.log(error);
    });
  };

  this.toggleStar = function(protoId, userId){
    return Star.toggleStar({protoId: protoId}).$promise
    .then(function(message){
      CoreService.toastSuccess(message.returnMsg);
      return message;
    })
    .catch(function(error){
      console.log(error);
    });
  };

  this.listProtocols = function(userId){
    return Star.find({filter:{where:{userId: userId}, include:'protocol'}}).$promise
    .then(function(stars){
      var protos = stars.map(function(a) {return a.protocol;});
      var filterprotos = protos.filter(function(e){return e});
      return filterprotos;
    })
    .catch(function(error){
      console.log(error);
    });
  };

  this.listUsers = function(protoId){
    return Star.find({filter:{where:{protocolId: protoId}, include:'user'}}).$promise
    .then(function(stars){
      console.log('protoId: ', protoId);
      console.log('stars: ', stars);
      var users = stars.map(function(a) {return a.user;});
      var filterusers = users.filter(function(e){return e});
      console.log("users from service: ", users);
      return filterusers;
    })
    .catch(function(error){
      console.log(error);
    });
  };

}]);
