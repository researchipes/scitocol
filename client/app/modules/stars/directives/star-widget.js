'use strict';
/*jshint unused: vars */
/**
 * @ngdoc directive
 * @name com.module.comments.directive:commentEditor
 * @description
 * # commentEditor
 */
angular.module('com.module.stars')
  .directive('starWidget',
  ['Star', 'StarsService', '$stateParams', 'CoreService','LoopBackAuth','User',
  function(Star, StarsService, $stateParams, CoreService, LoopBackAuth, User){

    return {
      templateUrl: 'modules/stars/directives/starWidgetTemplate.html',
      scope: {
        starcount: '=',
        starred: '=',
        protoId: '@'
      },

      link: function($scope, $element, $attrs){

        $scope.$watch('starcount',function(newValue, oldValue){
          $scope.starcount = newValue;
        });
        $scope.$watch('starred',function(newValue, oldValue){
          $scope.starred = newValue;
        });

        $scope.toggleStar = function(){
          StarsService.toggleStar($stateParams.id, LoopBackAuth.currentUserId)
          .then(function(res){
            return res;
          })
          .then(function(res2){
            return StarsService.myStar($stateParams.id,
              LoopBackAuth.currentUserId);
          })
          .then(function(res3){
            $scope.starred = res3;
            return StarsService.countStars($stateParams.id);
          })
          .then(function(res4){
            $scope.starcount = res4;
            return res4;
          })
          .catch(function(error){
            console.log(error);
          });

        };

        function init(){
          $scope.protoId = $stateParams.id;
          StarsService.myStar($stateParams.id,LoopBackAuth.currentUserId).
          then(function(result){
            $scope.starred = result;
          }, function(error){
            console.log(error);
          });

          StarsService.countStars($stateParams.id).then(function(result){
            console.log('myindex',$stateParams.id);
            $scope.starcount = result;
          }, function(error){
            console.log(error);
          });
        }

        init();

      }
    };
  }]);
