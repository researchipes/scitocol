'use strict';
/**
 * @ngdoc overview
 * @name loopbackApp
 * @description
 * # loopbackApp
 *
 * Main module of the application.
 */
angular.module('loopbackApp', [
    'ngTagsInput',
    'angular-loading-bar',
    'angular.filter',
    'angularBootstrapNavTree',
    'ngFileUpload',
    'ngImgCrop',
    'angular-img-cropper',
    'uuid',
    'luegg.directives',
    'hc.marked',
    'oitozero.ngSweetAlert',
    'config',
    'formly',
    'infinite-scroll',
    'wu.masonry',
    'lbServices',
    'monospaced.elastic',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'bootstrapLightbox',
    'ui.bootstrap',
    'ui.codemirror',
    'ui.gravatar',
    'ui.grid',
    'ui.router',
    'toasty',
    'autofields',
    'gettext',
    'as.sortable',
    'angularUtils.directives.dirPagination',
    'angularMoment',
    'monospaced.elastic',
    'com.module.core',
    'com.module.about',
    'com.module.settings',
    'com.module.users',
    'com.module.protocols',
    'com.module.commits',
    'com.module.files',
    'com.module.teams',
    'com.module.comments',
    'com.module.tags',
    'com.module.stars',
    'com.module.watchers'

  ])
  .run(function($rootScope, $cookies, gettextCatalog) {

    $rootScope.locales = {

      'en': {
        lang: 'en',
        country: 'US',
        name: gettextCatalog.getString('English')
      },
      'pt-BR': {
        lang: 'pt_BR',
        country: 'BR',
        name: gettextCatalog.getString('Portuguese Brazil')
      },
      'nl': {
        lang: 'nl',
        country: 'NL',
        name: gettextCatalog.getString('Dutch')
      },
      'de': {
        lang: 'de',
        country: 'DE',
        name: gettextCatalog.getString('German')
      },
      'fr': {
        lang: 'fr',
        country: 'FR',
        name: gettextCatalog.getString('Français')
      }
    }

    var lang = $cookies.lang || navigator.language || navigator.userLanguage;

    $rootScope.locale = $rootScope.locales[lang];

    if ($rootScope.locale === undefined) {
      $rootScope.locale = $rootScope.locales[lang];
      if ($rootScope.locale === undefined) {
        $rootScope.locale = $rootScope.locales['en'];
      }
    }

    gettextCatalog.setCurrentLanguage($rootScope.locale.lang);

    //Add deepExtend to the _ library
    _.mixin({deepExtend: underscoreDeepExtend(_)});

  });


